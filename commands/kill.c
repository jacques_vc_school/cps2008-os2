//
// Created by Jacques Vella Critien on 13/06/2020.
//


#include "kill.h"

int get_job_pid(int job_id, job_registry_t* job_registry)
{
    for(int i=0; i < job_registry->size; i++)
    {
        //if this job
        if(job_registry->jobs[i].id == job_id) {
            return job_registry->jobs[i].pid;
        }
    }

    return -1;
}

bool handle_kill_as_leader(kill_data_t kill_data, job_registry_t *global_job_registry)
{
    //byte ordering
    kill_data.job_id = ntohl(kill_data.job_id);

    //search for job to see if it exists
    //if it does not exist
    if(get_job_pid(kill_data.job_id, global_job_registry) < 0)
    {
        return false;
    }
    //if it exists
    else
    {
        char *hostname = get_job_hostname(global_job_registry, kill_data.job_id);
        int port = get_job_host_port(global_job_registry, kill_data.job_id);

        //holds connection to server
        connection_t server_conn;
        connect_to_server(&server_conn, hostname, port);

        //reconvert job id for nbo
        kill_data.job_id = htonl(kill_data.job_id);

        return send_kill_to_server(&server_conn, kill_data, false);
    }
}

void handle_kill_command(connection_t *conn, kill_data_t kill_data, pthread_mutex_t *lock, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry)
{
    int job_id =  ntohl(kill_data.job_id);
    int pid_to_kill = get_job_pid(job_id, personal_job_registry);
    ack_t ack;

    //if not on this server
    if(pid_to_kill < 0)
    {
        //if leader
        if(is_leader)
        {
            ack.status = handle_kill_as_leader(kill_data, global_job_registry);
        }
        else
        {
            //send kill to leader
            ack.status = send_kill_to_server(&leader->connection, kill_data, true);
        }

        check_result(write(conn->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING ACKNOWLEDGMENT FOR KILL COMMAND");
    }
    else
    {

        //get job status
        job_status_t job_status = get_job_status_from_id(personal_job_registry, job_id);

        //if running
        if(job_status == Running)
        {

            switch(kill_data.kill_mode)
            {
                case Soft: kill(pid_to_kill, SIGTERM);break;
                case Hard: kill(pid_to_kill, SIGKILL);break;
                case Nice:
                {
                    //try soft
                    kill(pid_to_kill, SIGTERM);
                    //sleep grace period
                    sleep(ntohl(kill_data.grace_period));

                    //check if it still exists
                    if(kill(pid_to_kill, 0) < 0)
                    {
                        //do hard
                        kill(pid_to_kill, SIGKILL);
                    }
                };break;
            }

            ack.status = true;
            if(write(conn->new_sock_fd, (void *) &ack, sizeof(ack)) < 0)
            {
                perror("ERROR WRITING BACK TO CLIENT");
            }

            update_job_status(job_id, Terminated, lock, is_leader, leader, global_job_registry, personal_job_registry);
        }
        else if(job_status == Scheduled)
        {

            ack.status = true;
            if(write(conn->new_sock_fd, (void *) &ack, sizeof(ack)) < 0)
            {
                perror("ERROR WRITING BACK TO CLIENT");
            }

            update_job_status(job_id, Terminated, lock, is_leader, leader, global_job_registry, personal_job_registry);
        }

    }
}

void validate_kill_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc)
{
    //make sure that at least 5 arguments were passed
    if(argc < 5){
        fprintf(stderr,"Not enough arguments\n");
        exit(EXIT_FAILURE);
    }

    //get hostname, port and command
    get_hostname_port_command(user_input, argv[2], user_input->command_type);

    //set job id
    command_to_send->data.kill_data.job_id = htonl(atoi(argv[3]));
    //set kill mode
    command_to_send->data.kill_data.kill_mode = get_kill_mode(argv[4]);

    //check if invalid kill mode
    if(command_to_send->data.kill_data.kill_mode == -1)
    {
        fprintf(stderr,"Invalid kill mode %s\n", argv[4]);
        exit(EXIT_FAILURE);
    }

    //set grace period
    command_to_send->data.kill_data.grace_period = htonl(0);

    //if nice kill mode
    if(command_to_send->data.kill_data.kill_mode == Nice)
    {
        //if grace period is specified
        if(argc == 6)
            command_to_send->data.kill_data.grace_period = htonl(atoi(argv[5]));
        else
            command_to_send->data.kill_data.grace_period = htonl(1);
    }
}


