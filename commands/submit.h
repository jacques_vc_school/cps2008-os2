//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_SUBMIT_H
#define CPS2008_OS2_SUBMIT_H

#include "../helpers/common.h"
#include "../helpers/server_helper.h"
#include "../helpers/client_helper.h"

/**
 * Method to handle a batch job
 * @param connection actual connection
 * @param job job to handle
 * @param log_path server's global log path
 */
void handle_batch_job(connection_t *connection,job_t *job, char *log_path);

/**
 * Method to validate user input for submit command
 * @param user_input holds user input
 * @param command_to_send command to send struct to be filled
 * @param argv arguments to program
 * @param argc number of arguments
 */
void validate_submit_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc);

#endif //CPS2008_OS2_SUBMIT_H
