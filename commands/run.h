//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_RUN_H
#define CPS2008_OS2_RUN_H

#include "../helpers/common.h"
#include "../helpers/server_helper.h"
#include "../helpers/client_helper.h"
#include <sys/termios.h>
#include <sys/select.h>
#include <sys/wait.h>

/**
 * Method for interactive mode for CLIENT side
 * @param args arguments to execute
 * @return function pointer
 */
void *input_interactive_data(void *args);

/**
 * Method to execute an interactive process for CLIENT
 * @param connection actual connection
 * @param std_in_fd connection for input
 * @param std_out_fd connection for output
 */
void execute_interactive_process(connection_t *connection, connection_t* std_in_fd, connection_t* std_out_fd);

/**
 * Method to handle an interactive job in a SERVER
 * @param connection connection details
 * @param received_command received command
 * @param job job to handle
 * @param log_path global log path of server
 * @param job_registry job registry to update
 * @param lock for locking
 */
void handle_interactive_job(connection_t *connection, command_t *received_command, job_t *job, char* log_path, job_registry_t *job_registry, pthread_mutex_t *lock);

/**
 * Method to validate user input for run command
 * @param user_input holds user input
 * @param command_to_send command to send struct to be filled
 * @param argv arguments to program
 * @param std_in_fd connection for input
 * @param std_out_fd connection for output
 */
void validate_run_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], connection_t *std_in_fd, connection_t *std_out_fd);

#endif //CPS2008_OS2_RUN_H
