//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "chdir.h"

void handle_chdir_command(connection_t * conn, char * chdir_path) {
    ack_t ack;
    //if change directory fails
    if (chdir(chdir_path) != 0)
    {
        //send false ack
        ack.status = false;
        check_result(write(conn->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING ACKNOWLEDGMENT FOR WRONG CHDIR COMMAND");
    }
    else
    {
        //send true ack
        ack.status = true;
        check_result(write(conn->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING ACKNOWLEDGMENT FOR GOOD CHDIR COMMAND");
    }

}

void validate_chdir_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc)
{
    //check if all needed arguments are passed
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments\n");
        exit(EXIT_FAILURE);
    }

    //get hostname, port and path
    get_hostname_port_command(user_input, argv[2], user_input->command_type);

    //copy path from input
    memset(command_to_send->data.command.actual_command, 0,256);
    memcpy(command_to_send->data.command.actual_command, user_input->command, strlen(user_input->command));

}