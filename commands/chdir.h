//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_CHDIR_H
#define CPS2008_OS2_CHDIR_H

#include "../helpers/common.h"
#include "../helpers/server_helper.h"
#include "../helpers/client_helper.h"

/**
 * Method to handle chdir command
 * @param conn server connection
 * @param chdir_path path to change to
 */
void handle_chdir_command(connection_t * conn, char * chdir_path);

/**
 * Method to validate user input for chdir command
 * @param user_input holds user input
 * @param command_to_send command to send struct to be filled
 * @param argv arguments to program
 * @param argc amount of arguments passed
 */
void validate_chdir_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc);


#endif //CPS2008_OS2_CHDIR_H
