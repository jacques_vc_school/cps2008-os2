//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "run.h"

/**
 *  why are we utilisng non-canonical mode for terminal?
 *  The reason for this stems from the fact that some commands may require further input
 *  and usually that requires processing the user input character by character. Please note that
 *  we are capturing some special characters (interrupt characters eg ctrl + c or ctrl + d)
 *  without having to fire a signal
 *  For a more in-depth explanation read Chapter 62: Terminals of the book
 *  The Linux Programming Interface: Unix system programming handbook.
 */
void *input_interactive_data(void *args)
{
    connection_t *std_in_fd = (connection_t *) args;

    // creating a set of FDs
    fd_set fdset;
    // setting all the elements of the set to 0
    FD_ZERO(&fdset);

    // setting the file descriptor that we are concerned with to true
    FD_SET(fileno(stdin), &fdset);

    // getting the attributes of the terminal
    struct termios term_settings, term_old;

    // saving the old terminal status to use it to revert to it after we are done
    check_result(tcgetattr(fileno(stdin), &term_old), "ERROR SAVING THE OLD TERMINAL");
    check_result(tcgetattr(fileno(stdin), &term_settings), "ERROR SAVING THE TERMINAL SETTING");

    // disabling canonical mode
    term_settings.c_lflag &= ~(ICANON);

    // how quickly do we want to flush the data
    // in the serial buffer
    term_settings.c_cc[VTIME] = 0;
    term_settings.c_cc[VMIN] = 0;

    // carry out the changes to the terminal immediately
    check_result(tcsetattr(fileno(stdin), TCSANOW, &term_settings), "ERROR CARRYING OUT CHANGES TO THE TERMINAL");
    printf("Press ctrl + d to terminate input \n");

    for(;;)
    {
        // Wait for at least one key entry in the buffer or timeout
        int ret = select(1, &fdset, NULL, NULL, NULL);
        check_result(ret, "CLIENT SELECT ERROR");

        // Read
        int key = fgetc(stdin);

        // Ctrl + D
        if (key == 0x04)
        {
            printf("Intercepted ctrl + d \n");
            // Close socket, to force EOF
            close(std_in_fd->new_sock_fd);

            // Restore terminal
            check_result(tcsetattr(fileno(stdin), TCSANOW, &term_old), "ERROR RESTORING TERMINAL");
            return NULL;
        }

        // Send a single character
        char character = (char) key;
        check_result(send(std_in_fd->new_sock_fd, &character, 1, 0), "ERROR SENDING CHARACTER");
    }
    return NULL;
}

void execute_interactive_process(connection_t *connection, connection_t* std_in_fd, connection_t* std_out_fd)
{
    //get size of structure which holds a structure
    socklen_t clilen = sizeof(struct sockaddr);

    //holds acknowledgement
    char ack = 1;

    //listen
    listen(std_in_fd->sockfd, 5);

    //listen
    listen(std_out_fd->sockfd, 5);

    //write acknowledgement and confirm it is sent
    check_result(write(connection->sockfd, (void*)&ack, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT FROM CLIENT TO SERVER");

    //accept connections on the input and output sockets
    std_in_fd->new_sock_fd = accept(std_in_fd->sockfd, (struct sockaddr *)&std_in_fd->client, &clilen);
    std_out_fd->new_sock_fd = accept(std_out_fd->sockfd, (struct sockaddr *)&std_out_fd->client, &clilen);

    //create a thread to accept input
    pthread_t input_thread;
    pthread_create(&input_thread, NULL, input_interactive_data, (void*)std_in_fd);

    //buffer for output
    char output_buffer[MAXLEN];

    printf("\n\n------- Output from server -------\n\n");

    for(;;) {
        int bytes_read = recv(std_out_fd->new_sock_fd, output_buffer, 256, MSG_DONTWAIT);

        if (bytes_read > 0) {
            output_buffer[bytes_read] = 0;
            printf("%s", output_buffer);
        }

        //if connection ends, cancel thread
        if (bytes_read == 0) {
            pthread_cancel(input_thread);
            break;
        }
    }

    printf("\n------- End -------\n\n");

    pthread_join(input_thread, NULL);
    printf("Connection with server ended\n");
    close(std_in_fd->sockfd);
    close(std_out_fd->new_sock_fd);
    close(std_out_fd->sockfd);
}

void handle_interactive_job(connection_t *connection, command_t *received_command, job_t *job, char* log_path, job_registry_t *job_registry, pthread_mutex_t *lock) {

    //create argument list
    char *args[MAXLEN + 1];
    //create structure for acknowledgement
    ack_t ack;
    //variable to hold the size read
    int size_read;
    //holds output to file status
    int error_output_status;
    char filename[MAXLEN];
    int socket = connection->new_sock_fd;

    sprintf(filename, "%s/job_%d_output.txt", log_path, job->id);
    FILE * file = fopen(filename, "a");

    //init file
    init_file(job, log_path);

    //fill and send acknowledgement to client
    ack.status = true;
    ack.job_id = htonl(job->id);
    check_result(write(connection->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR WRITING ACKNOWLEDGMENT TO CLIENT BEFORE INTERACTIVE");

    //holds the handshake sent by the client
    char sync_shake;

    //read acknowledgement from client
    size_read = read(connection->new_sock_fd, (void *) &sync_shake, sizeof(char));
    //check acknowledgment
    if(size_read <= 0)
    {
        //close socket
        close(connection->new_sock_fd);
        return;
    }

    //connections for input and output
    connection_t std_out_fd;
    connection_t std_in_fd;

    //set the server of the connections for input and output to the client
    std_out_fd.server = connection->client;
    std_in_fd.server = connection->client;

    //get reading port from command received
    int read_port = received_command->read_port;
    //gte writing port from command received
    int write_port = received_command->write_port;

    //add first port to input command
    std_in_fd.server.sin_port = htons(read_port);

    //add second port to output command
    std_out_fd.server.sin_port = htons(write_port);

    //check result and print to file any errors
    error_output_status = check_result_file_output("Handshake Error", size_read, file, true);
    //check output error status
    if(error_output_status < 0)
    {
        //close sockets
        close(connection->new_sock_fd);
        close(std_in_fd.sockfd);
        close(std_out_fd.sockfd);
        fclose(file);
        return;
    }

    //close file
    fclose(file);

    //process message received into arguments
    parse_command(received_command->actual_command, args);

    //create connections for input and output streams
    //std_in_fd  : CLIENT -> SERVER
    connect_conn(&std_in_fd);
    //std_out_df : SERVER -> CLIENT
    connect_conn(&std_out_fd);
    //close sockets on execute
    fcntl (std_in_fd.sockfd, F_SETFD, FD_CLOEXEC);
    fcntl (std_out_fd.sockfd, F_SETFD, FD_CLOEXEC);

    pid_t pid = fork();

    if(pid == 0)
    {
        //set up file
        char filename[MAXLEN];
        sprintf(filename, "%s/job_%d_output.txt", log_path, job->id);
        FILE * job_file = fopen(filename, "a");

        //storing outputs
        int in_output, out_output, err_output;
        in_output = dup(STDIN_FILENO);
        out_output = dup(STDOUT_FILENO);
        err_output = dup(STDERR_FILENO);

        check_result_file_output("Dup2 error redirecting input to input socket", dup2(std_in_fd.sockfd, STDIN_FILENO), job_file, false);
        check_result_file_output("Dup2 error redirecting output to output socket", dup2(std_out_fd.sockfd, STDOUT_FILENO), job_file, false);
        check_result_file_output("Dup2 error redirecting errors to output socket", dup2(std_out_fd.sockfd, STDERR_FILENO), job_file, false);
        check_result_file_output("Execvp error", execvp(args[0], args), job_file, false);
        //close file
        fclose(job_file);

        //restoring outputs
        dup2(in_output, STDIN_FILENO);
        dup2(out_output, STDOUT_FILENO);
        dup2(err_output, STDERR_FILENO);
    }
    else
    {
        update_job_pid(job->id, pid, job_registry, lock);
        waitpid(pid, NULL, 0);
        close(std_in_fd.sockfd);
        close(std_out_fd.sockfd);
        close(socket);
    }

}

void validate_run_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], connection_t *std_in_fd, connection_t *std_out_fd)
{
    command_to_send->data.command.now = true;

    //get hostname, port and command
    get_hostname_port_command(user_input, argv[2], user_input->command_type);

    //copy message from input
    memset(command_to_send->data.chdir_path, 0,256);
    memcpy(command_to_send->data.chdir_path, user_input->command, strlen(user_input->command));

    //set reading port for server
    command_to_send->data.command.read_port = reserve_port(std_in_fd);
    //set writing port for server
    command_to_send->data.command.write_port = reserve_port(std_out_fd);
}