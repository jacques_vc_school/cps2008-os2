//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_STATUS_H
#define CPS2008_OS2_STATUS_H

#include "../helpers/common.h"
#include "../helpers/client_helper.h"

/**
 * Method to print jobs in a registry
 * @param job_registry registry to print
 */
void print_registry(job_registry_t* job_registry);

/**
 * Method to get jobs in global registry
 * @param std_out_fd connection for output
 * @param leader boolean to show whether server is a leader
 * @param global_job_registry global registry to look for if leader
 * @param lock for locking
 * @return number of jobs returned, -1 if unsuccessul
 */
int get_jobs(connection_t *std_out_fd, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, pthread_mutex_t *lock);

/**
 * Method to execute a status command for CLIENT
 * @param connection  actual connection to server
 * @param std_out_fd connection for output
 */
void execute_status_command(connection_t *connection, connection_t* std_out_fd);

/**
 * Method to handle server command
 * @param conn connection of server
 * @param received_command received command
 * @param is_leader if is leader
 * @param leader leader structure
 * @param global_job_registry global job registry
 * @param lock for locking
 */
void handle_status_command(connection_t * conn, command_t *received_command, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, pthread_mutex_t *lock);


/**
 * Method to validate user input for status command
 * @param user_input holds user input
 * @param command_to_send command to send struct to be filled
 * @param argv arguments to program
 * @param argc amount of arguments passed
 * @param std_out_fd connection for output
 */
void validate_status_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc, connection_t *std_out_fd);

#endif //CPS2008_OS2_STATUS_H
