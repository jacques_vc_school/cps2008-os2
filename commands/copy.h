//
// Created by Jacques Vella Critien on 14/06/2020.
//

#ifndef CPS2008_OS2_COPY_H
#define CPS2008_OS2_COPY_H

#include "../helpers/common.h"
#include "../helpers/server_helper.h"

/**
 * Method to compress a folder and store the name of the compressed file
 * @param folder_name folder to compress
 * @param zip_filename where to store the name of the compressed file
 */
void zip_folder(char *folder_name, char *zip_filename);

/**
 * Method to unzip a folder and delete the zip file
 * @param folder_name the name of the folder being coped
 * @param new_filepath the new filepath of the folder copied
 * @param new_folder the name of the new directory
 */
void unzip_folder_file(char *folder_name, char *new_filepath, char *new_folder);

/**
 * Methdo to get hostname and port for copy command
 * @param user_input user input structure to fill
 * @param host_input acual user input arguments
 * @param argc amount of arguments
 * @param copy_transition type of copy transition
 */
void get_hostname_port_copy(user_input_t *user_input, char *host_input[], int argc, copy_transition_t copy_transition);

/**
 * Method to execute copy command from CLIENT
 * @param connection client's connection structure
 * @param copy_data copy details
 * @param ack acknowledgement returned by server
 * @param filenames filenames for copy
 */
void execute_copy_process(connection_t *connection, copy_data_t copy_data, ack_t ack, char *filenames[]);

/**
 * Method to handle copy command for SERVER
 * @param connection server's connection
 * @param received_data received data
 */
void handle_copy_command(connection_t *connection, copy_data_t *received_data);

/**
 * Method to validate user input for copy command
 * @param user_input holds user input
 * @param command_to_send command to send struct to be filled
 * @param argv arguments to program
 * @param argc number of arguments
 */
void validate_copy_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc);


#endif //CPS2008_OS2_COPY_H
