//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "submit.h"

void handle_batch_job(connection_t *connection,job_t *job, char *log_path) {

    //create structure for acknowledgement
    ack_t ack;
    //variable to hold the size read
    int size_read;

    //init file
    init_file(job, log_path);

    //fill and send acknowledgement to client
    ack.status = true;
    ack.job_id = htonl(job->id);
    check_result(write(connection->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR WRITING ACKNOWLEDGEMENT FOR BATCH JOB");

    //holds the handshake sent by the client
    char sync_shake;

    //read acknowledgement from client
    size_read = read(connection->new_sock_fd, (void *) &sync_shake, sizeof(char));
    //check acknowledgment
    if(size_read <= 0) {
        //close socket
        close(connection->new_sock_fd);
        return;
    }

    close(connection->new_sock_fd);
}

void validate_submit_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc) {
    //for submit date
    struct tm date;
    bzero(&date, sizeof(date));

    //check if there is a date passed
    if (argc > 3) {
        //if a date is specified and not now
        if (strcmp(argv[3], "now") != 0) {

            //check if all needed arguments are passed
            if (argc < 5) {
                fprintf(stderr, "Not enough arguments\n");
                exit(EXIT_FAILURE);
            }

            //read date
            if (strptime(argv[3], "%d/%m/%Y", &date) == NULL) {
                fprintf(stderr, "Error parsing date\n");
                exit(EXIT_FAILURE);
            }

            //read time
            if (strptime(argv[4], "%H:%M:%S", &date) == NULL) {
                fprintf(stderr, "Error parsing time\n");
                exit(EXIT_FAILURE);

            }

            command_to_send->data.command.now = false;
            //set date
            bzero(&command_to_send->data.command.date, sizeof(struct tm));
            date.tm_isdst = 0;

            //network byte ordering
            date.tm_year = htonl(date.tm_year);
            date.tm_mon = htonl(date.tm_mon);
            date.tm_mday = htonl(date.tm_mday);
            date.tm_hour = htonl(date.tm_hour);
            date.tm_min = htonl(date.tm_min);
            date.tm_sec = htonl(date.tm_sec);
            date.tm_isdst = htonl(date.tm_isdst);

            command_to_send->data.command.date = date;
        } else {
            command_to_send->data.command.now = true;
        }
    } else {
        command_to_send->data.command.now = true;
    }



    //get hostname, port and command
    get_hostname_port_command(user_input, argv[2], user_input->command_type);

    //copy message from input
    memset(command_to_send->data.chdir_path, 0,256);
    memcpy(command_to_send->data.chdir_path, user_input->command, strlen(user_input->command));
}