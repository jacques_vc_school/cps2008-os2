//
// Created by Jacques Vella Critien on 14/06/2020.
//

#include "copy.h"

void zip_folder(char *folder_name, char *zip_filename)
{
    char command[MAXLEN];
    strcpy(zip_filename, folder_name);
    strcat(zip_filename, ".zip");
    //zip file
    //> nul 2>nul suppresses output
    sprintf(command, "zip -r %s %s >/dev/null 2>/dev/null", zip_filename, folder_name);
    system(command);
}

void unzip_folder_file(char *folder_name, char *new_filepath, char *new_folder)
{
    char without_zip[MAXLEN];
    char to_move[MAXLEN];
    char to_remove[MAXLEN];
    char command[MAXLEN];
    strcpy(without_zip, folder_name);
    strtok(without_zip, ".zip");

    //if passed file contains a / move the created folders and remove the extra parent directory added in the front
    if(contains_char(without_zip, '/'))
    {
        strcpy(to_move, new_folder);
        strcat(to_move, "/");
        strcat(to_move, without_zip);
        strcpy(to_remove, new_folder);
        strcat(to_remove, "/");
        strcat(to_remove, strtok(folder_name, "/"));

        //unzip, change name and delete zip
        sprintf(command, "unzip -o %s -d %s >/dev/null 2>/dev/null && mv %s %s && rm %s && rm -r -d %s >/dev/null 2>/dev/null", new_filepath, new_folder,
                to_move, new_folder, new_filepath, to_remove);
    }
    else
    {
        //unzip and remove zip file
        sprintf(command, "unzip -o %s -d %s >/dev/null 2>/dev/null && rm %s >/dev/null 2>/dev/null", new_filepath, new_folder, new_filepath);
    }
    system(command);
}

void get_hostname_port_copy(user_input_t *user_input, char *host_input[], int argc, copy_transition_t copy_transition)
{
    char host_check[MAXLEN];
    int port_check;

    if(copy_transition == Server_to_Client)
    {
        user_input->port = -1;

        //read input
        sscanf(host_input[2], "%[^-]-%d", user_input->hostname, &user_input->port);

        //if no port was passed
        if(user_input->port == -1)
        {
            user_input->port = 5001;
        }

        //check other args
        for(int i=3; i<argc-1; i++)
        {

            //read input
            sscanf(host_input[i], "%[^-]-%d", host_check, &port_check);

            //if no port was passed
            if(port_check == -1)
            {
                port_check = 5001;

            }

            //if the host is not the same in all source directories
            if(strcmp(host_check, user_input->hostname) != 0 ||
               port_check != user_input->port)
            {
                fprintf(stderr,"Source directories or files are not stored on the same machine\n");
                exit(EXIT_FAILURE);
            }
        }

        //if the last file is also a remote file
        if(contain_a_colon(host_input[argc-1]))
        {
            fprintf(stderr,"The last path should be a local path\n");
            exit(EXIT_FAILURE);
        }

    }
    else if(copy_transition == Client_to_Server)
    {
        //make sure that the first files are all local
        for(int i=2; i<argc-1; i++)
        {
            //check if exists
            if(!file_exists(host_input[i]))
            {
                fprintf(stderr,"The file or directory %s does not exist\n", host_input[i]);
                exit(EXIT_FAILURE);
            }

            if(contain_a_colon(host_input[i]))
            {
                fprintf(stderr,"The source paths should be all local when copying from client to server\n");
                exit(EXIT_FAILURE);
            }
        }

        //get hostname and port from last directory
        char *input = host_input[argc-1];
        //read input
        sscanf(input, "%[^-]-%d", user_input->hostname, &user_input->port);

        //if no port was passed
        if(user_input->port == -1)
        {
            user_input->port = 5001;
        }
    }
}

void execute_copy_process(connection_t *connection, copy_data_t copy_data, ack_t ack, char *filenames[])
{
    //file pointer
    FILE *file;
    //buffer to fill when reading
    char buffer[MAXLEN];
    //used to check size
    struct stat st;
    //holds number of bytes read
    int rbytes = 0;
    //holds bytes read
    int rbytes_total = 0;
    //holds number of bytes written
    int wbytes = 0;
    //holds file details
    ftf_details_t file_details;
    //used to hold command
    char command[MAXLEN];

    if(!ack.status)
    {
        fprintf(stderr, "%s\n", ack.err_msg);
        return;
    }

    printf("Copying files...\n");

    //holds acknowledgement
    char handshake = 1;

    //if client to server
    if(copy_data.copy_transition == Client_to_Server)
    {
        //write acknowledgement and confirm it is sent
        check_result(write(connection->sockfd, (void*)&handshake, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT FROM CLIENT TO SERVER");

        //get number of files
        int number_of_files = ntohl(copy_data.copy_details.c2s_details.number_of_files);
        //struct to hold file details to send to server
        ftf_details_t file_details;

        //loop until all files are sent
        for(int i=0; i<number_of_files; i++)
        {
            char zip_filename[MAXLEN];
            bool is_dir = is_directory(filenames[i]);
            //check if directory
            if(is_dir)
            {
                file_details.file_type = Directory;

                //zip folder and get filename of compressed file
                zip_folder(filenames[i], zip_filename);
                memset(&st, 0, sizeof(st));
                stat(zip_filename, &st);
                file_details.size = htonl(st.st_size);
                strcpy(file_details.filename, zip_filename);

                //open file
                file = fopen(zip_filename, "r");
            }
            else
            {
                file_details.file_type = File;
                //fill file details
                strcpy(file_details.filename, get_after_char(filenames[i], '/'));
                memset(&st, 0, sizeof(st));
                stat(filenames[i], &st);
                file_details.size = htonl(st.st_size);

                //open file
                file = fopen(filenames[i], "r");
            }

            //send file details
            write(connection->sockfd, (void *) &file_details, sizeof(file_details));

            //clear buffer
            memset(buffer, 0, sizeof(buffer));

            //init wbytes to 0
            wbytes = 0;

            //write until all file has been sent
            while(wbytes < st.st_size)
            {
                rbytes = fread(buffer, 1, sizeof(buffer), file);
                wbytes += write(connection->sockfd, (void *) &buffer, rbytes);
                memset(buffer, 0, sizeof(buffer));
            }

            char sync_shake;
            //read acknowledgement from server
            if(read(connection->sockfd, (void *) &sync_shake, sizeof(char)) == 0)
            {
                fprintf(stderr, "Connection with server was lost");
                return;
            }
            //close file
            fclose(file);

            //if directory, delete zip
            if(is_dir)
            {
                memset(command, 0, sizeof(command));
                sprintf(command, "rm %s >/dev/null 2>/dev/null", zip_filename);
                system(command);
            }
        }
    }
    //if server to client
    if(copy_data.copy_transition == Server_to_Client)
    {
        //if more than 1 file to copy
        if(ntohl(copy_data.copy_details.s2c_details.number_of_files) > 1)
        {
            //check if file exists
            //if directory does not exist
            if (stat(filenames[0], &st) == -1) {
                //create directory and send unsuccessful ack if directory could not be created
                if(mkdir(filenames[0], 0700) < 0)
                {
                    fprintf(stderr, "Error creating directory");
                    //set handshake success to false
                    handshake = 0;
                    //write acknowledgement and confirm it is sent
                    check_result(write(connection->sockfd, (void*)&handshake, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT FROM CLIENT TO SERVER");
                    return;
                }
            }
        }

        //send handshake
        check_result(write(connection->sockfd, (void*)&handshake, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT FROM CLIENT TO SERVER");

        //get number of files to read
        int num_of_files = ntohl(copy_data.copy_details.s2c_details.number_of_files);

        //keep reading for all files
        for(int i=0; i < num_of_files; i++)
        {
            //init rbytes to 0
            rbytes =0;
            rbytes_total = 0;
            //read file details
            read(connection->sockfd, (void *) &file_details, sizeof(file_details));

            //holds new file path
            char new_filepath[MAXLEN];

            //if more than 1 file to copy
            if( ntohl(copy_data.copy_details.s2c_details.number_of_files) >1)
            {
                //fill new file path
                strcpy(new_filepath, filenames[0]);
                strcat(new_filepath, "/");
                strcat(new_filepath, get_after_char(file_details.filename, '/'));
            }
            else
            {

                //if it is a file, create the file
                if(file_details.file_type == File)
                {
                    file = fopen(filenames[0], "w");

                    if (file == NULL) {
                        fprintf(stderr, "File could not be created");
                        //set handshake to false
                        handshake = 0;
                        //write acknowledgement and confirm it is sent
                        check_result(write(connection->sockfd, (void*)&handshake, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT FROM CLIENT TO SERVER");
                        return;
                    }
                    strcpy(new_filepath, filenames[0]);
                }
                //if it is a directory
                else
                {

                    memset(&st, 0, sizeof(st));
                    //check if file exists
                    //if directory does not exist
                    if (stat(filenames[0], &st) == -1) {
                        //create directory and send unsuccessful ack if directory could not be created
                        if(mkdir(filenames[0], 0700) < 0)
                        {
                            fprintf(stderr, "Error creating directory");
                            //set handshake success to false
                            handshake = 0;
                            //write acknowledgement and confirm it is sent
                            check_result(write(connection->sockfd, (void*)&handshake, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT FROM CLIENT TO SERVER");
                            return;
                        }
                        strcpy(new_filepath, filenames[0]);
                        strcat(new_filepath, "/");
                        strcat(new_filepath, get_after_char(file_details.filename, '/'));
                    }
                }
            }

            //create file
            file = fopen(new_filepath, "w");
            //check file opening
            if (file == NULL) {
                fprintf(stderr, "Could not open file with path %s", new_filepath);
                close(connection->sockfd);
                return;
            }

            //read until all file has been filled
            while(rbytes_total < ntohl(file_details.size))
            {
                memset(buffer, 0, sizeof(buffer));
                rbytes = read(connection->sockfd, (void *) &buffer, sizeof(buffer));
                rbytes_total += rbytes;
                fwrite(buffer, 1, rbytes, file);
            }

            //write acknowledgement and confirm the file is ready
            char handshake = 1;
            if(write(connection->sockfd, (void*)&handshake, sizeof(char))<=0)
            {
                fprintf(stderr, "Connection with server was lost");
                return;
            }
            //close file pointer
            fclose(file);

            //if directory
            if(file_details.file_type == Directory)
            {
                //unzip folder and delete the zipfile
                unzip_folder_file(file_details.filename, new_filepath, filenames[0]);
            }
        }
    }

    printf("Files copied\n");
}

void handle_copy_command(connection_t *connection, copy_data_t *received_data) {

    //create structure for acknowledgement
    ack_t ack;
    //variable to hold the size read
    int size_read;
    //holds bytes read
    int rbytes = 0;
    //holds bytes read
    int rbytes_total = 0;
    //holds bytes written
    int wbytes;
    FILE * file;
    struct stat st;
    //to read of write from or to
    char buffer[MAXLEN];
    //struct to hold file_details when reading
    ftf_details_t file_details;
    //holds command for zipping and unzipping directories
    char command[MAXLEN];
    //holds zip filename
    char zip_filename[MAXLEN];

    //if client to server
    if(received_data->copy_transition == Client_to_Server)
    {
        //if more than 1 file being copied
        if(ntohl(received_data->copy_details.c2s_details.number_of_files) > 1)
        {
            //if directory does not exist
            if (stat(received_data->copy_details.c2s_details.directory, &st) == -1) {
                //create directory and send unsuccessful ack if directory could not be created
                if(mkdir(received_data->copy_details.c2s_details.directory, 0700) < 0)
                {
                    ack.status = false;
                    strcpy(ack.err_msg, "Directory could not be created");
                    check_result(write(connection->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR CREATING DIRECTORY");
                    return;
                }
            }
        }
    }
    //if server to client
    if(received_data->copy_transition == Server_to_Client)
    {
        //traverse all files to copy
        for(int i=0; i < ntohl(received_data->copy_details.s2c_details.number_of_files); i++)
        {

            //check if the file to read exists and if not, send an unsuccessful ack
            if(!file_exists(get_after_char(received_data->copy_details.s2c_details.files_to_copy[i], ':')))
            {
                ack.status = false;
                strcpy(ack.err_msg, "File does not exist");
                check_result(write(connection->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR FINDING FILE");
                return;
            }
        }
    }

    //fill and send acknowledgement to client
    ack.status = true;
    check_result(write(connection->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR WRITING ACKNOWLEDGMENT TO CLIENT BEFORE INTERACTIVE");

    //holds the handshake sent by the client
    char sync_shake;

    //read acknowledgement from client
    size_read = read(connection->new_sock_fd, (void *) &sync_shake, sizeof(char));
    //check acknowledgment
    if(size_read <= 0 || sync_shake == 0)
    {
        //close socket
        close(connection->new_sock_fd);
        return;
    }

    //if client to server
    if (received_data->copy_transition == Client_to_Server) {
        //get number of files to read
        int num_of_files = ntohl(received_data->copy_details.c2s_details.number_of_files);

        //keep reading for all filex
        for (int i = 0; i < num_of_files; i++) {
            //init rbytes to 0
            rbytes = 0;
            rbytes_total = 0;
            //read file details
            read(connection->new_sock_fd, (void *) &file_details, sizeof(file_details));

            //holds new file path
            char new_filepath[MAXLEN];

            //if more than 1 file to copy
            if(num_of_files>1)
            {
                //fill new file path
                strcpy(new_filepath, received_data->copy_details.c2s_details.directory);
                strcat(new_filepath, "/");
                strcat(new_filepath, get_after_char(file_details.filename, '/'));
            }
            else
            {
                //if it is a file, create the file
                if(file_details.file_type == File)
                {
                    //open file
                    file = fopen(received_data->copy_details.c2s_details.directory, "w");
                    //if cant open file
                    if (file == NULL) {
                        close(connection->new_sock_fd);
                        return;
                    }
                    strcpy(new_filepath, received_data->copy_details.c2s_details.directory);
                }
                //if it is a directory
                else
                {

                    memset(&st, 0, sizeof(st));
                    //if directory does not exist
                    if (stat(received_data->copy_details.c2s_details.directory, &st) == -1) {
                        //create directory and if it cannot be created cut connection
                        if(mkdir(received_data->copy_details.c2s_details.directory, 0700) < 0)
                        {
                            close(connection->new_sock_fd);
                            return;
                        }
                    }
                    //fill new file path
                    strcpy(new_filepath, received_data->copy_details.c2s_details.directory);
                    strcat(new_filepath, "/");
                    strcat(new_filepath, get_after_char(file_details.filename, '/'));
                }
            }

            //create file
            file = fopen(new_filepath, "w");
            //check file opening
            if (file == NULL) {
                close(connection->new_sock_fd);
                return;
            }

            //get actual file size
            file_details.size = ntohl(file_details.size);

            //read until all file has been filled
            while (rbytes_total < file_details.size) {
                memset(buffer, 0, sizeof(buffer));
                rbytes = read(connection->new_sock_fd, (void *) &buffer, sizeof(buffer));
                rbytes_total += rbytes;
                fwrite(buffer, 1, rbytes, file);
            }

            //write acknowledgement and confirm the file is ready
            char handshake = 1;
            if(write(connection->new_sock_fd, (void*)&handshake, sizeof(char))<=0)
            {
                fprintf(stderr, "Connection with client was lost");
                close(connection->new_sock_fd);
                fclose(file);
                return;
            }

            //close file pointer
            fclose(file);

            //if directory
            if(file_details.file_type == Directory)
            {
                //unzip folder and delete the zipfile
                unzip_folder_file(file_details.filename, new_filepath, received_data->copy_details.c2s_details.directory);
            }
        }

    }
    //if server to client
    else if (received_data->copy_transition == Server_to_Client) {
        //get number of files
        int number_of_files = ntohl(received_data->copy_details.s2c_details.number_of_files);
        //struct to hold file details to send to server
        ftf_details_t file_details;

        //loop until all files are sent
        for (int i = 0; i < number_of_files; i++) {
            //fill file details
            strcpy(file_details.filename,
                   get_after_char(received_data->copy_details.s2c_details.files_to_copy[i], ':'));

            //check if directory
            file_details.file_type = (is_directory(file_details.filename)) ? Directory : File;

            //set file name only actual name of file not path
            get_after_char(file_details.filename, '/');

            if(file_details.file_type == File)
            {

                memset(&st, 0, sizeof(st));
                //get file size
                stat(get_after_char(received_data->copy_details.s2c_details.files_to_copy[i], ':'), &st);
                file_details.size = htonl(st.st_size);
                //open file
                file = fopen(get_after_char(received_data->copy_details.s2c_details.files_to_copy[i], ':'), "r");
            }
            else if(file_details.file_type == Directory)
            {
                //zip folder and get filename of compressed file
                zip_folder(file_details.filename, zip_filename);

                memset(&st, 0, sizeof(st));
                stat(zip_filename, &st);
                file_details.size = htonl(st.st_size);
                strcpy(file_details.filename, zip_filename);
                //open file
                file = fopen(zip_filename, "r");
            }

            //send file details
            write(connection->new_sock_fd, (void *) &file_details, sizeof(file_details));

            //init wbytes to 0
            wbytes = 0;
            //clear buffer
            memset(buffer, 0, sizeof(buffer));

            //write until all file has been sent
            while (wbytes < st.st_size) {
                rbytes = fread(buffer, 1, sizeof(buffer), file);
                wbytes += write(connection->new_sock_fd, (void *) &buffer, rbytes);
                memset(buffer, 0, sizeof(buffer));
            }

            char sync_shake;
            //read acknowledgement from server
            if(read(connection->new_sock_fd, (void *) &sync_shake, sizeof(char)) == 0)
            {
                fprintf(stderr, "Connection with client was lost");
                close(connection->new_sock_fd);
                fclose(file);
                return;
            }
            //close file
            fclose(file);

            //if directory
            if(file_details.file_type == Directory)
            {
                memset(command, 0, sizeof(command));
                sprintf(command, "rm %s >/dev/null 2>/dev/null", zip_filename);
                system(command);
            }
        }
    }

    printf("Files copied\n");

    //close connection
    close(connection->new_sock_fd);
}

void validate_copy_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc)
{
    //make sure that at least 4 arguments were passed
    if(argc < 4){
        fprintf(stderr,"Not enough arguments\n");
        exit(EXIT_FAILURE);
    }

    bool is_first_server = contain_a_colon(argv[2]);
    bool is_last_server = contain_a_colon(argv[argc-1]);

    if(!(is_first_server ^ is_last_server))
    {
        fprintf(stderr,"One of the files must be remote or local\n");
        exit(EXIT_FAILURE);
    }

    if(is_first_server)
    {
        command_to_send->data.copy_data.copy_transition = Server_to_Client;

        //get number of files
        command_to_send->data.copy_data.copy_details.s2c_details.number_of_files = htonl(argc - 3);

        //get file names
        for(int i=2, j=0; i < argc-1; i++, j++)
        {
            strcpy(command_to_send->data.copy_data.copy_details.s2c_details.files_to_copy[j], argv[i]);
        }
    }
    else
    {
        command_to_send->data.copy_data.copy_transition = Client_to_Server;

        //get directory
        char filename_copy[MAXLEN];
        strcpy(filename_copy, argv[argc-1]);
        strcpy(command_to_send->data.copy_data.copy_details.c2s_details.directory, get_after_char(filename_copy, ':'));

        //get number of files
        command_to_send->data.copy_data.copy_details.c2s_details.number_of_files = htonl(argc - 3);
    }

    //get hostname, port and command
    get_hostname_port_copy(user_input, argv, argc, command_to_send->data.copy_data.copy_transition);

};