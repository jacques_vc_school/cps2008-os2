//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "status.h"

void print_registry(job_registry_t* job_registry)
{
    //to hold date
    char date[14];
    //to hold time
    char time[14];
    //to hold hostname
    char hostname[MAXLEN];

    if(job_registry->size > 0)
    {
        printf("%-10s%-20s%-15s%-10s%-15s%-15s%-15s\n", "Job", "Host", "Command", "Type", "Status", "Date", "Time");

        for(int i=0; i < job_registry->size;i++)
        {
            job_t job = job_registry->jobs[i];

            //get date and time
            sprintf(date, "%02d/%02d/%d", job.date.tm_mday, job.date.tm_mon+1, job.date.tm_year+1900);
            sprintf(time, "%02d:%02d:%02d", job.date.tm_hour, job.date.tm_min, job.date.tm_sec);
            sprintf(hostname, "%s:%d", job.hostname, job.port);
            printf("%-10d%-20s%-15s%-10s%-15s%-15s%-15s\n", job.id, hostname, job.command, get_job_type(job.type), get_job_status(job.status), date, time);
        }
    }
    else
    {
        printf("No jobs found!\n");
    }

}

int get_jobs(connection_t *std_out_fd, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, pthread_mutex_t *lock)
{
    pthread_mutex_lock(lock);

    //store original output
    int original_out = dup(STDOUT_FILENO);

    //if is leader
    if(is_leader)
    {
        //check dup2
        if(dup2(std_out_fd->sockfd, STDOUT_FILENO) < 0)
        {
            fprintf(stderr, "Failed to redirecting output to socket");
            pthread_mutex_unlock(lock);
            return -1;
        }

        //print global registry to socket
        print_registry(global_job_registry);

        //change output to original
        dup2(original_out, STDOUT_FILENO);

        //close socket
        close(std_out_fd->sockfd);

        pthread_mutex_unlock(lock);
        return global_job_registry->size;

    }
        //if not leader
    else
    {

        server_payload_t payload;
        payload.data.client_conn = *std_out_fd;
        payload.communication_type = Get;
        int wbytes = write(leader->connection.sockfd, (void*)&payload, sizeof(server_payload_t));
        //check write
        if(wbytes <=0)
        {
            fprintf(stderr,"LEADER IS OFF\n");

            pthread_mutex_unlock(lock);
            return -1;
        }
        //prepare struct for acknowledgement
        ack_t ack;

        //read acknowledgement
        int rbytes = read(leader->connection.sockfd, (void*) &ack, sizeof(ack));
        //check acknowledgement
        if(rbytes <=0)
        {
            fprintf(stderr,"LEADER IS OFF\n");
            pthread_mutex_unlock(lock);
            return -1;
        }

        pthread_mutex_unlock(lock);
        return ack.jobs_written;
    }


}

void execute_status_command(connection_t *connection, connection_t* std_out_fd)
{
    //get size of structure which holds a structure
    socklen_t clilen = sizeof(struct sockaddr);

    //holds acknowledgement
    char ack = 1;

    //listen
    listen(std_out_fd->sockfd, 5);

    //write acknowledgement and confirm it is sent
    check_result(write(connection->sockfd, (void*)&ack, sizeof(char)), "ERROR WRITING ACKNOWLEDGEMENT");

    //accept connections on the output socket
    std_out_fd->new_sock_fd = accept(std_out_fd->sockfd, (struct sockaddr *)&std_out_fd->client, &clilen);

    //buffer for output
    char output_buffer[MAXLEN];

    printf("\n\n------- Output from server --------\n\n");

    for(;;) {
        int bytes_read = recv(std_out_fd->new_sock_fd, output_buffer, 256, MSG_DONTWAIT);

        if (bytes_read > 0) {
            output_buffer[bytes_read] = 0;
            printf("%s", output_buffer);
        }

        //if connection ends
        if (bytes_read == 0) {
            break;
        }
    }

    printf("\n--------------- End ---------------\n\n");

    close(std_out_fd->new_sock_fd);
    close(std_out_fd->sockfd);
}

void handle_status_command(connection_t * conn, command_t *received_command, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, pthread_mutex_t *lock)
{
    ack_t ack;

    //fill and send acknowledgement to client
    ack.status = true;
    check_result(write(conn->new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING BACK ACKNOWLEDGMENT FOR STATUS COMMAND");

    //holds the handshake sent by the client
    char sync_shake;

    //read acknowledgement from client
    int size_read = read(conn->new_sock_fd, (void *) &sync_shake, sizeof(char));
    //check acknowledgment
    if(size_read <= 0)
    {
        //close socket
        close(conn->new_sock_fd);
        return;
    }

    //connections for input and output
    connection_t std_out_fd;

    //set the server of the connections for output to the client
    std_out_fd.server = conn->client;

    //gte writing port from command received
    int write_port = received_command->write_port;

    //add second port to output command
    std_out_fd.server.sin_port = htons(write_port);

    //create connections for output streams
    //std_out_df : SERVER -> CLIENT
    if(is_leader)
    {
        connect_conn(&std_out_fd);
        //close sockets on execute
        fcntl (std_out_fd.sockfd, F_SETFD, FD_CLOEXEC);
    }

    //get jobs
    int job_size = get_jobs(&std_out_fd, is_leader, leader, global_job_registry, lock);
    if(job_size < 0)
    {
        close(leader->connection.sockfd);
        close(conn->new_sock_fd);
        exit(EXIT_FAILURE);
    }

}

void validate_status_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc, connection_t *std_out_fd)
{
    //check if all needed arguments are passed
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments\n");
        exit(EXIT_FAILURE);
    }

    //get hostname, port and command
    get_hostname_port_command(user_input, argv[2], user_input->command_type);

    //set writing port for server
    command_to_send->data.command.write_port = reserve_port(std_out_fd);
}
