//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_KILL_H
#define CPS2008_OS2_KILL_H

#include "../helpers/common.h"
#include "../helpers/server_helper.h"
#include "../helpers/client_helper.h"
#include <signal.h>

/**
 * Function to get the pid of a job
 * @param job_id the id of the job
 * @param job_registry the registry in which to check
 * @return the pid of the job
 */
int get_job_pid(int job_id, job_registry_t* job_registry);

/**
 * Method to handle_kill_as_leader
 * @param kill_data  the data to perform kill
 * @param global_job_registry global registry to check in
 * @return true if successful
 */
bool handle_kill_as_leader(kill_data_t kill_data, job_registry_t *global_job_registry);

/**
 * Method to handle a kill command
 * @param conn connection of server
 * @param kill_data the data to perform kill
 * @param lock for locking
 * @param is_leader to hold if the server is a leader
 * @param leader to holds leader structure if a leader
 * @param global_job_registry global job registry
 * @param personal_job_registry server's own registry
 */
void handle_kill_command(connection_t * conn, kill_data_t kill_data, pthread_mutex_t *lock, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry);

/**
 * Method to validate user input for kill command
 * @param user_input holds user input
 * @param command_to_send command to send struct to be filled
 * @param argv arguments to program
 * @param argc number of arguments
 */
void validate_kill_command(user_input_t *user_input, payload_t *command_to_send, char* argv[], int argc);

#endif //CPS2008_OS2_KILL_H
