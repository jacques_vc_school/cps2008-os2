//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_LEADER_HELPER_H
#define CPS2008_OS2_LEADER_HELPER_H

#include "common.h"
#include "../commands/status.h"
#include "../commands/kill.h"
#include "server_helper.h"
#include "client_helper.h"
#include <arpa/inet.h>

/**
 * Method to terminate a host jobs
 * @param host host whose jobs to terminate
 * @param global_job_registry registry from which to remove jobs
 */
void terminate_host_jobs(char *host, job_registry_t *global_job_registry);

/**
 * Method to handle a server connection
 * @param connection connection details
 * @param handshake_details handshake details
 * @param lock for locking
 * @param is_leader to hold if server is leader
 * @param global_job_registry global job registry
 * @param personal_job_registry server's registry
 */
void handle_server(connection_t *connection, server_leader_handshake_t *handshake_details, pthread_mutex_t *lock, bool is_leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry);


/**
 * Method to connect to leader
 * @param connection server's connection
 * @param leader leader to connect to
 */
void connect_to_leader(connection_t *connection, leader_t *leader);

/**
 * Method to fill port and host in leader structure
 * @param host_string string from where to get data
 * @param leader leader struct to fill
 */
void get_port_host(char *host_string, leader_t *leader);

#endif //CPS2008_OS2_LEADER_HELPER_H
