//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "client_helper.h"

void get_hostname_port_command(user_input_t *user_input, char *host_input, int command_type)
{
    char hostname[MAXLEN];
    char command_input[MAXLEN];
    int server_port = -1;

    //read input
    sscanf(host_input, "%[^-]-%d:%[^\t\n]", hostname, &server_port, command_input);

    //checking if host is passed
    if(hostname[0] == ':')
    {
        fprintf(stderr,"No server name passed\n");
        exit(0);
    }

    //copy hostname
    strcpy(user_input->hostname, hostname);

    //if the lengths are equal it means there is no port
    if(server_port == -1)
    {
        user_input->port = 5001;
    }
    else
    {
        user_input->port = server_port;
    }

    //if it is not a status command
    if(command_type != 3 && command_type != 4)
    {
        //checking if command is passed
        if(strlen(command_input) == 0)
        {
            if(command_type != 6)
                fprintf(stderr,"No command passed\n");
            else
                fprintf(stderr,"No path passed\n");

            exit(0);
        }

        //copy actual command
        strcpy(user_input->command, command_input);
    }

}

int reserve_port(connection_t *connection)
{
    //initiate socket
    connection->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    //make sure it is initiated
    check_result(connection->sockfd, "ERROR CREATING SOCKET FOR RESERVATION OF PORT");
    //set option to be reusable
    set_no_linger(connection);

    //set server
    connection->server.sin_family = AF_INET;
    connection->server.sin_addr.s_addr = INADDR_ANY;
    connection->server.sin_port = 0;

    //check bind of socket with server
    check_result(bind(connection->sockfd, (const struct sockaddr *) &connection->server, sizeof(connection->server)), "ERROR BINDING SOCKET FOR RESERVATION OF PORT");

    //get port
    struct sockaddr_in sin;
    socklen_t len = sizeof(sin);
    //check get sock name
    if (getsockname(connection->sockfd, (struct sockaddr *)&sin, &len) == -1)
        perror("GETTING GENERATED PORT:");

    return ntohs(sin.sin_port);
}

void connect_to_server(connection_t *connection, char *server_hostname, int server_port)
{
    //struct to hold server address
    struct hostent *server;

    // Create a socket
    if ((connection->sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("ERROR opening socket");
        exit(EXIT_FAILURE);
    }

    // Get server name
    if ((server = gethostbyname(server_hostname)) == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        close(connection->sockfd);
        exit(EXIT_FAILURE);
    }

    // Populate server structure
    bzero((char *) &connection->server, sizeof(connection->server));
    // Set to AF_INET
    connection->server.sin_family = AF_INET;
    // Set server address
    bcopy((char *) server->h_addr,
          (char *) &connection->server.sin_addr.s_addr,
          server->h_length);
    // Set port (convert to network byte ordering)
    connection->server.sin_port = htons(server_port);

    // Connect to the server
    if (connect(connection->sockfd, (struct sockaddr *) &connection->server, sizeof(connection->server)) < 0)
    {
        perror("ERROR connecting");
        close(connection->sockfd);
        exit(EXIT_FAILURE);
    }
}