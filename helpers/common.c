//
// Created by Jacques Vella Critien on 29/05/2020.
//
#include "common.h"


bool contains(char *source, char *substring)
{
    //get the part
    char* sub = strstr(source, substring);
    if(sub)
        return true;
    else
        return false;
}

void str_to_lower(char *s)
{
    for(int i=0; i < strlen(s); i++)
    {
        s[i] = tolower(s[i]);
    }

}

int get_command_type(char* command)
{
    //get command in lower case
    str_to_lower(command);

    if(strcmp("run", command) == 0)
        return 1;
    else if(strcmp("submit", command) == 0)
        return 2;
    else if(strcmp("status", command) == 0)
        return 3;
    else if(strcmp("kill", command) == 0)
        return 4;
    else if(strcmp("copy", command) == 0)
        return 5;
    else if(strcmp("chdir", command) == 0)
        return 6;
    else
    {
        fprintf(stderr,"Invalid command %s\n", command);
        exit(0);
    }
}

kill_mode_t get_kill_mode(char* mode)
{

    //get mode in lower case
    str_to_lower(mode);

    if(strcmp("soft", mode) == 0)
        return Soft;
    else if(strcmp("hard", mode) == 0)
        return Hard;
    else if(strcmp("nice", mode) == 0)
        return Nice;
    else
        return -1;

}

int check_result_file_output(char *message, int result, FILE *file, bool read_or_write)
{
    //if result smaller than 0
    if(result == 0 && read_or_write)
    {
        fprintf(file, "%s: Read or write returned nothing\n", message);
        return -1;
    }
    if(result < 0)
    {
        printf("ERROR: %s\n", strerror(errno));
        fprintf(file, "%s: %s\n", message, strerror(errno));
        return -1;
    }
    return 0;
}

void check_result(int result, char *message)
{
    //if result smaller than 0
    if(result < 0)
    {
        perror(message);
        exit(1);
    }
}

void set_no_linger(connection_t *conn_details)
{
    struct linger linger_details;
    // block until data is transmitted
    linger_details.l_onoff = 1;
    linger_details.l_linger = 0;

    // specifying what should happens to a socket in case there is untransmitted data and the socket is closed
    check_result(setsockopt(conn_details->sockfd, SOL_SOCKET,SO_REUSEADDR,(const void *)&linger_details, sizeof(struct linger)), "ERROR SETTING SOCKET OPTIONS");
}


void bind_conn(connection_t *connection, int server_port)
{
    //initiate socket
    connection->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    //make sure it is initiated
    check_result(connection->sockfd, "ERROR CREATING SOCKET");
    //set option to be reusable
    set_no_linger(connection);

    //set server
    connection->server.sin_family = AF_INET;
    connection->server.sin_addr.s_addr = INADDR_ANY;
    connection->server.sin_port = htons(server_port);

    //check bind of socket with server
    check_result(bind(connection->sockfd, (const struct sockaddr *) &connection->server, sizeof(connection->server)), "ERROR BINDING SOCKET");

}

void connect_conn(connection_t *connection)
{
    connection->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    check_result(connection->sockfd, "ERROR CREATING SOCKET");

    check_result(connect(connection->sockfd, (struct sockaddr *) &connection->server, sizeof(connection->server)), "ERROR CONNECTING SOCKET");
}

bool contains_char(char *str, char to_check)
{
    for(int i=0; i<strlen(str); i++)
        if(str[i] == to_check)
            return true;

    return false;
}

bool contain_a_colon(char *str)
{
    return contains_char(str, ':');
}

int is_directory(const char *path) {
    struct stat buf;
    if (stat(path, &buf)!=0)
        return 0;
    return S_ISDIR(buf.st_mode);
}

int file_exists(const char *path)
{
    struct stat stats;

    stat(path, &stats);

    // Check for file existence
    if (stats.st_mode)
        return 1;

    return 0;
}

char * get_after_char(char *str, char delim)
{
    //check if the string contains the delimeter
    if(contains_char(str, delim))
    {
        char *tokenized = strrchr(str, delim);
        return (tokenized+1);
    }
    //if not, return the original string
    else
        return str;

}