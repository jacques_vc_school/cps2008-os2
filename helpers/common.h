//
// Created by Jacques Vella Critien on 29/05/2020.
//

#ifndef CPS2008_OS2_COMMON_H
#define CPS2008_OS2_COMMON_H


#include "registry.h"
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <pthread.h>
#include <unistd.h>

#define MAX_FILES_COPY 10

//enum for communication purpose between server and leader
enum communication_purpose{Add, Update, Get, Kill};
typedef enum communication_purpose communication_purpose_t;

//enum for kill mode
enum kill_mode{Soft, Hard, Nice};
typedef enum kill_mode kill_mode_t;

//enum to hold if file or directory
enum file_type{File, Directory};
typedef enum file_type file_type_t;

/**
 * Struct to hold input command details
 */
typedef struct user_input {
    //actual command
    char command[MAXLEN];
    //command type
    int command_type;
    //hostname
    char hostname[MAXLEN];
    //port
    int port;
} user_input_t;

/**
 * Handshake structure for communication between server and leader
 */
typedef struct server_leader_handshake
{
    //holds hostname
    char hostname[MAXLEN];
    //holds hostname port;
    int port;
} server_leader_handshake_t;

typedef struct command {
    //actual command
    char actual_command[MAXLEN];
    //reading port for the server
    int read_port;
    //writing port for the server
    int write_port;
    //holds date
    struct tm date;
    //holds whether to execute it now
    bool now;
} command_t;

/**
 * Struct to hold interactive job details
 */
typedef struct connection
{
    int sockfd;
    struct sockaddr_in server;
    struct sockaddr_in client;
    int new_sock_fd;
} connection_t;

/**
 * Struct for acknowledgement
 */
typedef struct ack
{
    bool status;
    char err_msg[MAXLEN];
    int job_id;
    int jobs_written;
} ack_t;

/**
 * Struct for leader
 */
typedef struct leader{
    //hostname
    char hostname[MAXLEN];
    //port
    int port;
    //connection to leader
    connection_t connection;
} leader_t;

/**
 *  Structure for sending data for kill mode
 */
typedef struct kill_data
{
 int job_id;
 kill_mode_t kill_mode;
 int grace_period;
} kill_data_t;

//enum for copy transition
enum copy_transition {Client_to_Server, Server_to_Client};
typedef enum copy_transition copy_transition_t;

/**
 * Holds copy details for a file to file copy
 */
typedef struct ftf_details
{
    char filename[MAXLEN];
    size_t size;
    file_type_t file_type;
} ftf_details_t;

/**
 * Holds copy details for multiple files to directory copy for CLIENT TO SERVER
 */
typedef struct c2s_copy_details
{
    char directory[MAXLEN];
    int number_of_files;
} c2s_copy_details_t;

/**
 * Holds copy details for multiple files to directory copy for CLIENT TO SERVER
 */
typedef struct s2c_copy_details
{
    char files_to_copy[MAX_FILES_COPY][MAXLEN];
    int number_of_files;
} s2c_copy_details_t;


/**
 * Holds copy details for multiple files to directory copy
 */
union copy_details
{
    c2s_copy_details_t c2s_details;
    s2c_copy_details_t s2c_details;
};

typedef struct copy_data
{
    // this shows whether the copy will be from the
    // client to the server or vise versa
    copy_transition_t copy_transition;
    // this holds the details about the copy
    union copy_details copy_details;

} copy_data_t;


/**
 * Union for the payload data to send for communication
 */
union payload_data
{
    //command
    command_t command;
    //server leader handshake
    server_leader_handshake_t handshake;
    //kill mode data
    kill_data_t kill_data;
    //path
    char chdir_path[MAXLEN];
    //copy data
    copy_data_t copy_data;
};

/**
 * Struct which represents payload of data for communication between servers and clients
 */
typedef struct payload
{
    //actual data
    union payload_data data;
    //flag to see if client or server
    bool server;
    //command type
    int command_type;
} payload_t;

/**
 * Structure for data to be used for an update of a job
 */
typedef struct job_update_data
{
    //job id to update
    int job_id;
    //job status
    job_status_t job_status;
} job_update_t;

/**
 * Union for the server payload data, it can either be a job in the case of adding
 * or a job update data structure containing id and new status
 * or a connection when getting the jobs
 */
union server_payload_data
{
    job_t new_job;
    job_update_t update_data;
    kill_data_t kill_data;
    connection_t client_conn;
};

/**
 * Struct which represents payload of data for communication between servers and leaders
 */
typedef struct server_payload
{
    //actual data
    union server_payload_data data;
    //flag to see if it is an update
    communication_purpose_t communication_type;
} server_payload_t;

/**
 * Method to get command type number from string
 * @param command command as input
 * @return run - 1
 */
int get_command_type(char* command);

/**
 * Sets socket option to close if there is no input
 * @param conn_details connection
 */
void set_no_linger(connection_t *conn_details);

/**
 * Method to change a string to lower case
 * @param s string to change
 * @return string in lowercase
 */
void str_to_lower(char *s);

/**
 * Function to check if a source contains a sequence
 * @param source source to check
 * @param substring sequence
 * @return true if it contains
 */
bool contains(char *source, char *substring);

/**
 * Method to get kill mode from user input
 * @param mode user input
 * @return kill mode
 */
kill_mode_t get_kill_mode(char* mode);

/**
 * Method to check result and print to file if an error occurs
 * @param message message to print before error
 * @param result result to check
 * @param read_or_write whether this will check a read or write
 * @param file file pointer
 */
int check_result_file_output(char *message, int result, FILE *file, bool read_or_write);

/**
 * Method to check the result of a function call
 * @param result to check
 * @param message message to output
 */
void check_result(int result, char *message);

/**
 * Method to bind connection
 * @param connection connection to fill
 * @param server_port server's port
 */
void bind_conn(connection_t *connection, int server_port);

/**
 * Method to connect connection to its socket
 * @param connection connection to connect
 */
void connect_conn(connection_t *connection);

/**
 * Function to check if a string contains a colon
 * @param str to check
 * @return true if the string contains a colon
 */
bool contain_a_colon(char *str);

/**
 * Method to check if a string contains a specific character
 * @param str string to check
 * @param to_check character to look for
 * @return true if found
 */
bool contains_char(char *str, char to_check);

/**
 * Function to check if a path is a directory or not
 * @param path path to check
 * @return > 0 if a directory
 */
int is_directory(const char *path);

/**
 * Function to check if a file or directory exists
 * @param path path to check
 * @return >  0 if exists
 */
int file_exists(const char *path);

/**
 * Function to get the second part of a string after the delimeter
 * @param str string to check
 * @param delim delimeter
 * @return second part of str after delimeter
 */
char * get_after_char(char *str, char delim);

#endif //CPS2008_OS2_COMMON_H