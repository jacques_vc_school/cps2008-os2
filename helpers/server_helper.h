//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_SERVER_HELPER_H
#define CPS2008_OS2_SERVER_HELPER_H

#include "common.h"
#include <string.h>
#include <arpa/inet.h>

/**
 * Method to parse command
 * @param input_command command to parse
 * @param arguments arguments to fill
 */
void parse_command(char *input_command, char **arguments);

/**
 * Method to initialise a log file for a job
 * @param job the job for which a file will be created
 * @param log_path global log path
 */
void init_file(job_t *job, char *log_path);

/**
 * Method to update a job's pid
 * @param job_id job to update
 * @param pid new pid
 * @param job_registry job registry to update
 * @param lock lock to set
 */
void update_job_pid(int job_id, int pid, job_registry_t *job_registry, pthread_mutex_t *lock);


/**
 * Method to update a job status
 * @param job_id job to update
 * @param status new status
 * @param lock for locking
 * @param is_leader to hold if the server is a leader
 * @param leader to holds leader structure if a leader
 * @param global_job_registry global job registry
 * @param personal_job_registry server's own registry
 * @return -1 if unsuccessful
 */
int update_job_status(int job_id, job_status_t status, pthread_mutex_t *lock, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry);


/**
 * Method to clear job registries
 * @param is_leader if is a leader
 * @param global_job_registry global job registris to clear
 * @param personal_job_registry server's own registry to clear
 */
void clear_jobs(bool is_leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry);

/**
 * Function to send job to leader and get job id
 * @param job job to send
 * @param lock for locking
 * @param is_leader to hold if the server is a leader
 * @param leader to holds leader structure if a leader
 * @param global_job_registry global job registry
 * @param personal_job_registry server's own registry
 * @return new job id if successful, -1 if not
 */
int send_job_get_id(job_t *job, pthread_mutex_t *lock, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry);

/**
 * Method to fill job
 * @param connection server's connection
 * @param new_job job struct to fill
 * @param received_payload data received
 */
void fill_job(connection_t *connection, job_t *new_job, payload_t received_payload);

/**
 * Method to forward kill to leader if job is not found
 * @param connection server's connection
 * @param kill_data kill details to forward
 * @param to_leader whether the server to send to is a leader
 * @return ack success status
 */
bool send_kill_to_server(connection_t *connection, kill_data_t kill_data, bool to_leader);

#endif //CPS2008_OS2_SERVER_HELPER_H
