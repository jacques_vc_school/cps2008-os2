//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "leader_helper.h"

void terminate_host_jobs(char *host, job_registry_t *global_job_registry)
{
    //variable to host current job host
    char current_job_host[MAXLEN];

    //traverse all personal jobs to find jobs
    for(int i=0; i < global_job_registry->size; i++)
    {
        //get job's hostname and port
        sprintf(current_job_host, "%s:%d", global_job_registry->jobs[i].hostname, global_job_registry->jobs[i].port);
        //if the job's host is the same one as the one passed
        if(strcmp(current_job_host, host) == 0) {
            //update status
            global_job_registry->jobs[i].status = Terminated;
        }
    }
}

void handle_server(connection_t *connection, server_leader_handshake_t *handshake_details, pthread_mutex_t *lock, bool is_leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry)
{
    //holds data to be received
    server_payload_t received_payload;
    //send back unsuccessful handshake
    ack_t ack;
    //holds job
    job_t job;
    //to store connection
    connection_t conn = *connection;
    //to store copy of the address of the server being handles
    char address[MAXLEN];
    //store address
    sprintf(address, "%s:%d", handshake_details->hostname, handshake_details->port);

    //print connection details
    fprintf(stderr,
            "New connection from %s:%d.\n",
            inet_ntoa(connection->client.sin_addr),
            handshake_details->port);


    //fill and send acknowledgement to server
    ack.status = is_leader;
    check_result(write(conn.new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR WRITING ACKNOWLEDGEMENT TO SERVER FROM LEADER");

    //if not a leader return;
    if(!is_leader)
        return;

    //read forever from server
    while(1)
    {
        //read job
        int rbytes = read(conn.new_sock_fd, (void*) &received_payload, sizeof(server_payload_t));
        //if client is turned off
        if(rbytes ==0)
        {
            printf("\n-----------------------\n\n");
            printf("SERVER DISCONNECTED\n\n");
            fprintf(stderr,"SERVER %s WAS TURNED OFF\n", address);
            terminate_host_jobs(address, global_job_registry);
            close(conn.new_sock_fd);
            pthread_cancel(pthread_self());
        }

        //if bytes are read
        if(rbytes > 0)
        {
            //if it is an add
            if(received_payload.communication_type == Add)
            {
                //update global job registry size
                global_job_registry->size++;
                //get job from payload
                job = received_payload.data.new_job;
                //set job id
                job.id = global_job_registry->size;

                //lock mutex
                pthread_mutex_lock(lock);

                //realloc space for jobs
                if((global_job_registry->jobs = (job_t *)realloc(global_job_registry->jobs, global_job_registry->size*sizeof(job_t))) == NULL)
                {
                    //check if failed
                    fprintf(stderr,"Cannot reallocate memory for global jobs\n");
                    exit(EXIT_FAILURE);
                }
                //set job
                (*global_job_registry).jobs[global_job_registry->size-1] = job;

                //unlock mutex
                pthread_mutex_unlock(lock);

                //set ack
                ack.status = true;
                ack.job_id = htonl(job.id);

                //write back to server with job id
                check_result(write(conn.new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING BACK THE NEW JOB ID");
            }
            else if(received_payload.communication_type == Update)
            {
                job.id = ntohl(received_payload.data.update_data.job_id);
                //traverse all personal jobs to find jobs
                for(int i=0; i < global_job_registry->size; i++)
                {
                    //if this job
                    if(global_job_registry->jobs[i].id == job.id) {
                        //update status
                        global_job_registry->jobs[i].status = received_payload.data.update_data.job_status;
                    }
                }
                ack.status = true;

                //write back to server with job id
                check_result(write(conn.new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING ACKNOWLEDGEMENT AFTER UPDATE");
            }
            else if(received_payload.communication_type == Get)
            {
                //store original output
                int original_out = dup(STDOUT_FILENO);
                //get connection
                connection_t *server_conn = &received_payload.data.client_conn;

                //print connection details
                fprintf(stderr,
                        "client connection %s:%d.\n",
                        inet_ntoa(server_conn->server.sin_addr),
                        handshake_details->port);

                //get the client's address
                char *client_addr =  inet_ntoa(server_conn->server.sin_addr);
                printf("client addr: %s\n", client_addr);

                //if it was local to the server making the request, use the address of the server
                if(strcmp("127.0.0.1", client_addr) == 0)
                {
                    inet_pton(AF_INET,  inet_ntoa(connection->client.sin_addr), &server_conn->server.sin_addr);
                }

                server_conn->server.sin_family = AF_INET;

                //connect socket to client
                connect_conn(server_conn);

                //check dup2
                if(dup2(server_conn->sockfd, STDOUT_FILENO) < 0)
                {
                    fprintf(stderr, "Failed to redirect output to socket\n");
                    //set ack
                    ack.status = false;

                    //write back to server with job id
                    check_result(write(conn.new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR REDIRECTING OUTPUT TO CLIENT SOCKET FOR STATUS");
                    continue;
                }

                //print global registry to socket
                print_registry(global_job_registry);

                //change output to original
                dup2(original_out, STDOUT_FILENO);

                //close socket
                close(server_conn->sockfd);

                //set ack
                ack.status = true;
                ack.jobs_written = htonl(personal_job_registry->size);

                //write back to server with job id
                check_result(write(conn.new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR WRITING BACK TO SERVER FROM LEADER");
            }
            else if(received_payload.communication_type == Kill)
            {
                ack.status = handle_kill_as_leader(received_payload.data.kill_data, global_job_registry);
                //write back to server with job id
                check_result(write(conn.new_sock_fd, (void *) &ack, sizeof(ack)), "ERROR SENDING ACKNOWLEDGEMENT AFTER UPDATE");

            }
        }
    }
}

void connect_to_leader(connection_t *connection, leader_t *leader)
{
    //structure for payload
    payload_t to_send;

    //connect to leader
    connect_to_server(&leader->connection, leader->hostname, leader->port);

    //set data to send
    to_send.server = true;
    //set handshake details
    to_send.data.handshake.port = htons(connection->server.sin_port);
    strcpy(to_send.data.handshake.hostname, inet_ntoa(connection->server.sin_addr));

    int wbytes = write(leader->connection.sockfd, (void*)&to_send, sizeof(to_send));
    //check write
    check_result(wbytes, "ERROR WRITING TO LEADER");

    //prepare struct for acknowledgement
    ack_t ack;

    //read acknowledgement
    int rbytes = read(leader->connection.sockfd, (void*) &ack, sizeof(ack));
    //check acknowledgement
    check_result(rbytes, "ERROR READING FROM LEADER");

    //if status is false, it means that leader is not an actual leader
    if(!ack.status)
    {
        fprintf(stderr, "Host %s:%d is not an actual leader!\n", leader->hostname, leader->port);
        exit(EXIT_FAILURE);
    }

    printf("Connected to leader!\n");

}

void get_port_host(char *host_string, leader_t *leader)
{
    //host length
    size_t host_len = strlen(host_string);
    //tokenize host
    char *tokenized = strtok(host_string, "-");

    //copy hostname
    strcpy(leader->hostname, tokenized);

    //if the lengths are equal it means there is no port
    if(strlen(tokenized) == host_len)
    {
        //set port
        leader->port = 5001;
    }
    else
    {
        //get port
        tokenized = strtok(NULL, "-");
        leader->port = atoi(tokenized);
    }
}
