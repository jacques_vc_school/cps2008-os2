//
// Created by Jacques Vella Critien on 03/06/2020.
//

#ifndef CPS2008_OS2_REGISTRY_H
#define CPS2008_OS2_REGISTRY_H

#define __USE_XOPEN
#define _GNU_SOURCE
#include <time.h>
#include <sys/types.h>
#include <stdio.h>

#define MAXLEN 256

//enum for job status
enum job_status{Scheduled, Running, Terminated};
//enum for job type
enum job_type{I, B};


typedef enum job_status job_status_t;

typedef enum job_type job_type_t;

/**
 * Struct for job
 */
typedef struct job
{
    //job id
    int id;
    //host
    char hostname[MAXLEN];
    //port
    int port;
    //command
    char command[MAXLEN];
    //job type
    job_type_t type;
    //job status
    job_status_t status;
    //date
    struct tm date;
    //process id
    int pid;
} job_t;

typedef struct job_registry
{
    //jobs
    job_t *jobs;
    //size
    int size;
} job_registry_t;

/**
 * Method to get actual enum type from int type
 * @param type type as in t
 *     1 - INTERATIVE
 *     2 - BATCH
 * @return job type
 */
enum job_type get_type(int type);

/**
 * Method to get the job type form enum
 * @param job_type job type to get
 * @return string representation of job type
 */
char *get_job_type(job_type_t job_type);

/**
 * Method to get the job status form enum
 * @param job_status job status to get
 * @return string representation of job status
 */
char *get_job_status(job_status_t job_status);

/**
 * Method to get the job status of a particular job
 * @param job_registry job registry to check
 * @param id job's id
 * @return the status of the job
 */
job_status_t get_job_status_from_id(job_registry_t *job_registry, int job_id);

/**
 * Method to get job host's port
 * @param job_registry job registry to search
 * @param job_id job id to get
 * @return job host's port
 */
int get_job_host_port(job_registry_t *job_registry, int job_id);

/**
 * Method to get job's hostname and port
 * @param job_registry job registry to search
 * @param job_id job id to get
 * @return job's hostname
 */
char *get_job_hostname(job_registry_t *job_registry, int job_id);

#endif //CPS2008_OS2_REGISTRY_H
