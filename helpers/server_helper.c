//
// Created by Jacques Vella Critien on 13/06/2020.
//

#include "server_helper.h"

void parse_command(char *input_command, char **arguments)
{
    int token_count;
    char *command = input_command;
    token_count = 0;

    for(token_count = 0; token_count < 32; token_count++)
    {
        if((arguments[token_count] = strtok(command, " \t")) == NULL)
        {
            break;
        }

        command = NULL;
    }

    arguments[token_count] = NULL;

}

void init_file(job_t *job, char *log_path)
{
    //file pointer for job file
    FILE *job_file;
    //variable to hold filename
    char filename[MAXLEN];

    sprintf(filename, "%s/job_%d_output.txt", log_path, job->id);

    //open file to write or create it
    job_file = fopen(filename, "w");

    //If file opening went wrong
    if(job_file == NULL)
    {
        /* File not created hence exit */
        fprintf(stderr, "Unable to create file.\n");
    }

    //get now's time and date
    time_t now;
    time(&now);
    struct tm *date = localtime(&now);

    //Append filename, job type and command
    fprintf(job_file, "Job ID: %d\n"
                      "Job Type: Interactive\n"
                      "Command: %s\n"
                      "Command submitted at %02d/%02d/%d %02d:%02d:%02d\n", job->id, job->command, date->tm_mday, date->tm_mon+1,
            date->tm_year+1900, date->tm_hour, date->tm_min, date->tm_sec);

    if(job->type == B)
    {
        struct tm* job_date = &job->date;
        //print scheduled time
        fprintf(job_file, "Command scheduled to run at %02d/%02d/%d %02d:%02d:%02d\n", job_date->tm_mday, job_date->tm_mon+1,
                job_date->tm_year+1900, job_date->tm_hour, job_date->tm_min, job_date->tm_sec);
    }


    //print ending line
    fprintf(job_file, "-----------------------------------------\n\n");

    fclose(job_file);
}

void update_job_pid(int job_id, int pid, job_registry_t *job_registry, pthread_mutex_t *lock)
{
    pthread_mutex_lock(lock);
    //traverse all personal jobs to find jobs
    for(int i=0; i < job_registry->size; i++)
    {
        //if this job
        if(job_registry->jobs[i].id == job_id) {
            job_registry->jobs[i].pid = pid;
        }
    }
    pthread_mutex_unlock(lock);
}

int update_job_status(int job_id, job_status_t status, pthread_mutex_t *lock, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry)
{
    int count;

    pthread_mutex_lock(lock);

    //if leader, update global job registry
    if(is_leader)
    {
        //traverse all personal jobs to find jobs
        for(count=0; count < global_job_registry->size; count++)
        {
            //if this job
            if(global_job_registry->jobs[count].id == job_id) {
                global_job_registry->jobs[count].status = status;
            }
        }
    }
        //if not leader
    else
    {
        //prepare data to send to leader
        server_payload_t payload;
        payload.data.update_data.job_id = htonl(job_id);
        payload.data.update_data.job_status = status;
        payload.communication_type = Update;

        int wbytes = write(leader->connection.sockfd, (void*)&payload, sizeof(server_payload_t));
        if(wbytes <=0)
        {
            fprintf(stderr,"LEADER IS OFF\n");
            return -1;
        }

        //prepare struct for acknowledgement
        ack_t ack;

        //read acknowledgement
        int rbytes = read(leader->connection.sockfd, (void*) &ack, sizeof(ack));
        //check acknowledgement
        if(rbytes <=0)
        {
            fprintf(stderr,"LEADER IS OFF\n");
            return -1;
        }
    }

    //traverse all personal jobs to find jobs
    for(count=0; count < personal_job_registry->size; count++)
    {
        //if this job
        if(personal_job_registry->jobs[count].id == job_id) {
            //if terminated
            if (status == Terminated) {
                //reduce size
                personal_job_registry->size--;
                //break from loop
                break;
            }
            //if running
            if(status == Running)
            {
                //change status
                personal_job_registry->jobs[count].status = Running;
            }
        }
    }

    //if terminated
    if(status == Terminated)
    {
        //if size is 0, dont reallocate memory just clear the first job
        if(personal_job_registry->size == 0)
            memset(&personal_job_registry->jobs[count], 0, sizeof(job_t));
        else
        {
            //move elements one space backwards
            for (int j=count; j<personal_job_registry->size; j++)
                personal_job_registry->jobs[j] = personal_job_registry->jobs[j+1];

            //realloc to reduce size
            if ((personal_job_registry->jobs = (job_t *) realloc(personal_job_registry->jobs,
                                                                 personal_job_registry->size * sizeof(job_t))) ==
                NULL) {
                //check if failed
                fprintf(stderr, "Cannot reallocate memory for jobs\n");
                exit(EXIT_FAILURE);
            }
        }

    }

    pthread_mutex_unlock(lock);

    return 0;
}

void clear_jobs(bool is_leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry)
{
    //if leader
    if(is_leader)
    {
        free(global_job_registry->jobs);
        free(global_job_registry);
    }

    //free server's own registry
    free(personal_job_registry->jobs);
    free(personal_job_registry);
}

int send_job_get_id(job_t *job, pthread_mutex_t *lock, bool is_leader, leader_t *leader, job_registry_t *global_job_registry, job_registry_t *personal_job_registry)
{
    pthread_mutex_lock(lock);

    //if is leader
    if(is_leader)
    {

        //update global job registry size
        global_job_registry->size++;
        //set job id
        job->id = global_job_registry->size;

        //realloc space for jobs
        if((global_job_registry->jobs = (job_t *)realloc(global_job_registry->jobs, global_job_registry->size*sizeof(job_t))) == NULL)
        {
            //check if failed
            fprintf(stderr,"Cannot reallocate memory for global jobs\n");
            exit(EXIT_FAILURE);
        }
        //set job
        (*global_job_registry).jobs[global_job_registry->size-1] = *job;
    }
        //if not leader
    else
    {
        server_payload_t payload;
        payload.data.new_job = *job;
        payload.communication_type = Add;
        int wbytes = write(leader->connection.sockfd, (void*)&payload, sizeof(server_payload_t));
        //check write
        if(wbytes <=0)
        {
            fprintf(stderr,"LEADER IS OFF\n");
            return -1;
        }

        //prepare struct for acknowledgement
        ack_t ack;

        //read acknowledgement
        int rbytes = read(leader->connection.sockfd, (void*) &ack, sizeof(ack));
        //check acknowledgement
        if(rbytes <=0)
        {
            fprintf(stderr,"LEADER IS OFF\n");
            return -1;
        }

        //get id from acknowledgement
        job->id = ntohl(ack.job_id);
    }

    //update size
    personal_job_registry->size++;
    //realloc space for jobs
    if((personal_job_registry->jobs = (job_t *)realloc(personal_job_registry->jobs, personal_job_registry->size*sizeof(job_t))) == NULL)
    {
        //check if failed
        fprintf(stderr,"Cannot reallocate memory for jobs\n");
        exit(EXIT_FAILURE);
    }
    //set job
    (*personal_job_registry).jobs[personal_job_registry->size-1] = *job;
    pthread_mutex_unlock(lock);

    printf("Job %d created!\n", job->id);
    return job->id;
}

void fill_job(connection_t *connection, job_t *new_job, payload_t received_payload)
{
    //set type
    new_job->type = get_type(received_payload.command_type);
    //set status
    //if interactive

    if(received_payload.command_type == 1)
        new_job->status = Running;
        //if batch
    else if(received_payload.command_type == 2)
        new_job->status = Scheduled;
    //set command
    strcpy(new_job->command, received_payload.data.command.actual_command);
    //set port
    new_job->port = ntohs(connection->server.sin_port);
    //set command
    strcpy(new_job->command, received_payload.data.command.actual_command);
    //get hostname
    strcpy(new_job->hostname, inet_ntoa(connection->server.sin_addr));

    //if now was specified
    if(received_payload.data.command.now)
    {
        time_t now;
        time(&now);
        new_job->date = *localtime(&now);
    }
    else
    {
        //set date
        //network byte ordering
        received_payload.data.command.date.tm_year = ntohl(received_payload.data.command.date.tm_year);
        received_payload.data.command.date.tm_mon = ntohl(received_payload.data.command.date.tm_mon);
        received_payload.data.command.date.tm_mday = ntohl(received_payload.data.command.date.tm_mday);
        received_payload.data.command.date.tm_hour = ntohl(received_payload.data.command.date.tm_hour);
        received_payload.data.command.date.tm_min = ntohl(received_payload.data.command.date.tm_min);
        received_payload.data.command.date.tm_sec = ntohl(received_payload.data.command.date.tm_sec);
        received_payload.data.command.date.tm_isdst = ntohl(received_payload.data.command.date.tm_isdst);

        new_job->date = received_payload.data.command.date;
    }
}

bool send_kill_to_server(connection_t *connection, kill_data_t kill_data, bool to_leader)
{
    //holds written bytes
    int wbytes;

    //if sending to leader
    if(to_leader)
    {
        //prepare kill command to forward to leader
        server_payload_t payload;
        payload.communication_type = Kill;

        //set job id
        payload.data.kill_data.job_id = htonl(ntohl(kill_data.job_id));
        //set kill mode
        payload.data.kill_data.kill_mode = htonl(ntohl(kill_data.kill_mode));
        //set grace period
        payload.data.kill_data.grace_period = htonl(ntohl(kill_data.grace_period));

        wbytes = write(connection->sockfd, (void*)&payload, sizeof(server_payload_t));
    }
    else
    {
        //prepare kill command to forward to another server
        payload_t payload;
        payload.command_type = htons(4);

        //set job id
        payload.data.kill_data.job_id = htonl(ntohl(kill_data.job_id));
        //set kill mode
        payload.data.kill_data.kill_mode = htonl(ntohl(kill_data.kill_mode));
        //set grace period
        payload.data.kill_data.grace_period = htonl(ntohl(kill_data.grace_period));

        wbytes = write(connection->sockfd, (void*)&payload, sizeof(payload_t));
    }

    //check write
    if(wbytes <=0)
    {
        fprintf(stderr,"LEADER IS OFF\n");
        return -1;
    }

    //prepare struct for acknowledgement
    ack_t ack;

    //read acknowledgement
    int rbytes = read(connection->sockfd, (void*) &ack, sizeof(ack));
    //check acknowledgement
    if(rbytes <=0)
    {
        fprintf(stderr,"LEADER IS OFF\n");
        return -1;
    }

    //return status
    return ack.status;
}
