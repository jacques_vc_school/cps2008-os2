//
// Created by Jacques Vella Critien on 03/06/2020.
//

#include "registry.h"

enum job_type get_type(int type)
{
    switch (type) {
        case 1: return I;
        default: return B;
    }
}

char *get_job_type(job_type_t job_type)
{
    switch(job_type)
    {
        case I: return "I";
        case B: return "B";
        default: return NULL;
    }

}

char *get_job_status(job_status_t job_status)
{
    switch(job_status)
    {
        case Scheduled: return "SCHEDULED";
        case Running: return "RUNNING";
        case Terminated: return "TERMINATED";
        default: return NULL;
    }
}

job_status_t get_job_status_from_id(job_registry_t *job_registry, int job_id)
{
    for(int i=0; i < job_registry->size; i++)
    {
        //if this job
        if(job_registry->jobs[i].id == job_id) {
            return job_registry->jobs[i].status;
        }
    }

    return -1;
}

int get_job_host_port(job_registry_t *job_registry, int job_id)
{
    for(int i=0; i < job_registry->size; i++)
    {
        //if this job
        if(job_registry->jobs[i].id == job_id) {
            return job_registry->jobs[i].port;
        }
    }

    return -1;
}

char *get_job_hostname(job_registry_t *job_registry, int job_id)
{
    for(int i=0; i < job_registry->size; i++)
    {
        //if this job
        if(job_registry->jobs[i].id == job_id) {
            return job_registry->jobs[i].hostname;
        }
    }

    return NULL;
}
