//
// Created by Jacques Vella Critien on 13/06/2020.
//

#ifndef CPS2008_OS2_CLIENT_HELPER_H
#define CPS2008_OS2_CLIENT_HELPER_H

#include "common.h"
#include <netdb.h>

/**
 * Method to get the hostname, port and command from input
 * @param userInput structure to fill
 * @param host_input input from user
 * @param command_type command type
 */
void get_hostname_port_command(user_input_t *user_input, char *host_input, int command_type);

/**
 * Method to reserve port and return port number
 * @param connection connection to store
 * @return port number
 */
int reserve_port(connection_t *connection);

/**
 * Function to connect to a server and set connection
 * @param connection connection to set
 * @param server_hostname server's hostname
 * @param server_port server's port
 * @return -1 if unsuccessful
 */
void connect_to_server(connection_t *connection, char *server_hostname, int server_port);

#endif //CPS2008_OS2_CLIENT_HELPER_H
