#include "commands/run.h"
#include "commands/submit.h"
#include "commands/status.h"
#include "commands/kill.h"
#include "commands/chdir.h"
#include "commands/copy.h"
#include "helpers/server_helper.h"
#include "helpers/leader_helper.h"

//holds connection
connection_t connection;
//struct to hold leader
leader_t leader;
//to hold if leader
bool is_leader = false;
//holds this server's job registry
job_registry_t *personal_job_registry;
//holds global server's job registry IF LEADER
job_registry_t *global_job_registry;
//lock
pthread_mutex_t lock;
//holds path to log folder
char log_path[MAXLEN];


/**
 * Signal handler for signal interrupts
 * @param just_an_int dummy var
 */
void signal_handler(int just_an_int){
    exit(EXIT_SUCCESS);
}

/**
 * Exit handler for when exit is called
 */
void exit_handler(void) {
    clear_jobs(is_leader, global_job_registry, personal_job_registry);
    close(connection.sockfd);
    printf("Closing server\n");
}

/**
 * Method to execute batch job
 * @param batch_job batch job to execute
 * @return void pointer - THREAD FUNCTION
 */
void *execute_batch_job(void *batch_job)
{
    //get job from param
    job_t *job = (job_t *)batch_job;

    //create argument list
    char *args[MAXLEN + 1];
    //variable to hold filename
    char filename[MAXLEN];
    sprintf(filename, "%s/job_%d_output.txt", log_path, job->id);

    //process message received into arguments
    parse_command(job->command, args);

    pid_t pid = fork();
    if(pid == 0)
    {
        //open file pointer and print output
        FILE * job_file_ptr = fopen(filename, "a");
        fprintf(job_file_ptr, "Output from server\n\n");

        //set up file
        char filename[MAXLEN];
        sprintf(filename, "%s/job_%d_output.txt", log_path, job->id);
        int job_file = open(filename, O_RDWR | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);

        printf("Child process executing the command \n");
        check_result_file_output("Dup2 error redirecting output to output socket", dup2(job_file, STDOUT_FILENO), job_file_ptr, false);
        check_result_file_output("Dup2 error redirecting errors to output socket", dup2(job_file, STDERR_FILENO), job_file_ptr, false);
        //close file
        fclose(job_file_ptr);
        close(job_file);

        check_result_file_output("Execvp error", execvp(args[0], args), job_file_ptr, false);

    }
    else
    {
        update_job_pid(job->id, pid, personal_job_registry, &lock);
        waitpid(pid, NULL, 0);
        //set terminated
        int update_status = update_job_status(job->id, Terminated, &lock, is_leader, &leader, global_job_registry, personal_job_registry);
        if(update_status < 0)
        {
            close(leader.connection.sockfd);
            exit(EXIT_FAILURE);
        }

    }
    return NULL;

}

/**
 * Method to loop through jobs until a job is found
 * @return nothing infinite loop - THREAD FUNC
 */
_Noreturn void *check_batch_jobs() {

    while(1)
    {
        //adding sleep for helping the CPU
        sleep(1);
        //traverse all personal jobs to find jobs
        for(int i=0; i < personal_job_registry->size; i++)
        {
            //if a job is scheduled
            if(personal_job_registry->jobs[i].status == Scheduled) {

                //get current time
                time_t now;
                time(&now);

                //for issue of hours
                struct tm job_tm;
                job_tm.tm_isdst = 0;
                bzero(&job_tm, sizeof(struct tm));
                job_tm = personal_job_registry->jobs[i].date;

                job_tm.tm_isdst = -1;
                //get job's time
                time_t job_time = mktime(&job_tm);

                //get the difference
                double diffSecs = difftime(now, job_time);

                //if job_time is before or now
                if (diffSecs >= 0)
                {
                    //set job to running
                    personal_job_registry->jobs[i].status = Running;
                    //create thread to handle batch job
                    pthread_t batch_job_thd;

                    pthread_create( &batch_job_thd, NULL ,execute_batch_job, (void *)&personal_job_registry->jobs[i]);
                }
            }
        }
    }

}

/**
 * Method to handle a new communication
 * @param connection_arg communication details
 * @return pointer for thread
 */
void *handle_connection(void *connection_arg) {

    //holds received command
    payload_t received_payload;
    connection_t *conn = (connection_t *)connection_arg;

    //read data
    check_result(read(conn->new_sock_fd, (void *) &received_payload, sizeof(payload_t)), "ERROR READING PAYLOAD");

    //if a client
    if(!received_payload.server)
    {
        printf("New Job\n\n");

        //print connection details
        fprintf(stderr,
                "New connection from %s:%d.\n",
                inet_ntoa(conn->client.sin_addr),
                htons(conn->client.sin_port));

        //switch to host byte ordering
        received_payload.command_type = ntohs(received_payload.command_type);

        //If a status command
        if(received_payload.command_type == 3)
        {
            handle_status_command(conn, &received_payload.data.command, is_leader, &leader, global_job_registry, &lock);
        }
        //if kill
        else if(received_payload.command_type == 4)
        {
            handle_kill_command(conn, received_payload.data.kill_data, &lock, is_leader, &leader, global_job_registry, personal_job_registry);
        }
        //if copy
        else if(received_payload.command_type == 5)
        {
            handle_copy_command(conn, &received_payload.data.copy_data);
        }
        //if chdir
        else if(received_payload.command_type == 6)
        {
            handle_chdir_command(conn, received_payload.data.chdir_path);
        }
        //if not the above
        else
        {
            //holds new job
            job_t *new_job = (job_t*)malloc(sizeof(job_t));

            //fill job details
            fill_job(conn, new_job, received_payload);

            //send job to leader, get id and set job to list of jobs
            int job_id =send_job_get_id(new_job, &lock, is_leader, &leader, global_job_registry, personal_job_registry);
            if(job_id < 0)
            {
                close(leader.connection.sockfd);
                close(conn->new_sock_fd);
                exit(EXIT_FAILURE);
            }

            //if interactive
            if(new_job->type == I)
            {
                //handle interactive job
                handle_interactive_job(conn, &received_payload.data.command, new_job, log_path, personal_job_registry, &lock);

                //set terminated
                int update_status = update_job_status(job_id, Terminated, &lock, is_leader, &leader, global_job_registry, personal_job_registry);
                if(update_status < 0)
                {
                    close(leader.connection.sockfd);
                    close(conn->new_sock_fd);
                    exit(EXIT_FAILURE);
                }
            }
            //if batch
            else
            {
                //handle batch job
                handle_batch_job(conn, new_job, log_path);
            }

            //free job
            free(new_job);
        }
    }
    //if a server
    else
    {

        printf("Server Connection\n\n");

        //create new thread for client
        handle_server(conn, &received_payload.data.handshake, &lock, is_leader, global_job_registry, personal_job_registry);

    }

    return NULL;
}

int main(int argc, char *argv[]) {

    //signals
    signal(SIGINT, signal_handler);
    signal(SIGTSTP, signal_handler);
    //exit handler
    atexit(exit_handler);

    struct stat st;
    //if directory does not exist, create it
    if (stat("logs", &st) == -1) {
        mkdir("logs", 0700);
    }

    //get realpath of logs directory
    realpath("logs", log_path);

    //to hold length of socket
    socklen_t size;
    //to hold port
    int port;

    //init job registry
    personal_job_registry = (job_registry_t *)malloc(sizeof(job_registry_t));
    personal_job_registry->size = 0;
    personal_job_registry->jobs = (job_t *)malloc(sizeof(job_t)* personal_job_registry->size);

    //if not enough arguments
    if (argc < 2) {
        fprintf(stderr, "Not enough arguments\n");
        exit(EXIT_FAILURE);
    }

    //traverse all args
    for (int i = 0; i < argc; i++) {

        //if first argument
        if (i == 1) {
            //if port is passed, use port
            if (contains(argv[i], "-p")) {
                port = atoi(argv[1] + 3);

                //if not enough arguments
                if (argc < 3) {
                    fprintf(stderr, "No leader is passed\n");
                    exit(EXIT_FAILURE);
                }
            }
                //else use 5001 as default
            else {
                port = 5001;
            }
            //bind connection
            bind_conn(&connection, port);
        }


        //if there is a -l
        if (contains(argv[i], "-l")) {
            //get leader details
            //if argument is -l, this should be the leader
            if (strcmp(argv[i], "-l") == 0) {
                is_leader = true;
                //initialise global job registry
                global_job_registry = (job_registry_t *)malloc(sizeof(job_registry_t));
                global_job_registry->size = 0;
                global_job_registry->jobs = (job_t *)malloc(sizeof(job_t)* global_job_registry->size);
            } else {
                get_port_host(argv[i] + 3, &leader);
                connect_to_leader(&connection, &leader);
            }
        }
    }

    //listen
    listen(connection.sockfd, 5);

    if(is_leader)
        printf("LEADER listening on port %d\n", port);
    else
        printf("SERVER listening on port %d\n", port);

    //create thread to handle batch jobs
    pthread_t batch_thd;
    pthread_create( &batch_thd, NULL ,check_batch_jobs, NULL);

    while (1) {
        //holds size of client
        size = sizeof(connection.client);
        //accept and validate connection
        connection.new_sock_fd = accept(connection.sockfd, (struct sockaddr *) &connection.client, &size);
        //check accept
        if (connection.new_sock_fd < 0) {
            close(connection.sockfd);
            perror("ACCEPT ERROR");
            exit(EXIT_FAILURE);
        }

        printf("\n-----------------------\n\n");
        printf("NEW CONNECTION\n\n");

        pthread_t client_thd;
        pthread_create( &client_thd, NULL ,  handle_connection , (void*)&connection);
    }
}