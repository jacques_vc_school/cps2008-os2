#include "commands/run.h"
#include "commands/submit.h"
#include "commands/status.h"
#include "commands/copy.h"
#include "commands/kill.h"
#include "commands/chdir.h"
#include "helpers/client_helper.h"


int main(int argc, char *argv[])
{
    //holds client's connection
    connection_t *connection;

    //input struct
    user_input_t user_input;

    //payload to send
    payload_t command_to_send;
    //connection for input
    connection_t std_in_fd;
    //connection for output
    connection_t std_out_fd;

    //allocate memory for connection
    connection = (connection_t *)malloc(sizeof(connection_t));

    // If there is no command
    if (argc < 2) {
        fprintf(stderr,"Not enough arguments\n");
        free(connection);
        exit(EXIT_FAILURE);
    }

    //get command type
    user_input.command_type = get_command_type(argv[1]);

    //validate input
    switch (user_input.command_type) {
        //run
        case 1: validate_run_command(&user_input, &command_to_send, argv, &std_in_fd, &std_out_fd);break;
        //submit
        case 2: validate_submit_command(&user_input, &command_to_send, argv, argc);break;
        //status
        case 3: validate_status_command(&user_input, &command_to_send, argv, argc, &std_out_fd);break;
        //kill
        case 4: validate_kill_command(&user_input, &command_to_send, argv, argc);break;
        //copy
        case 5: validate_copy_command(&user_input, &command_to_send, argv, argc);break;
        //chdir
        case 6: validate_chdir_command(&user_input, &command_to_send, argv, argc);break;
        default:
        {
            fprintf(stderr,"Incorrect command type\n");
            free(connection);
            exit(EXIT_FAILURE);
        }
    }

    //connect to server
    connect_to_server(connection, user_input.hostname, user_input.port);
    printf("Connected to server %s:%d!\n", user_input.hostname, user_input.port);

    //set as client
    command_to_send.server = false;
    //copy command type
    command_to_send.command_type = htons(user_input.command_type);

    int wbytes = write(connection->sockfd, (void*)&command_to_send, sizeof(command_to_send));
    //check write
    check_result(wbytes, "ERROR WRITING TO SERVER");

    //prepare struct for acknowledgement
    ack_t ack;

    //read acknowledgement
    int rbytes = read(connection->sockfd, (void*) &ack, sizeof(ack));
    //check acknowledgement
    check_result(rbytes, "ERROR READING FROM SERVER");
    if(rbytes > 0)
    {
        //if interactive
        if(user_input.command_type == 1)
            execute_interactive_process(connection, &std_in_fd, &std_out_fd);
        //if status
        else if(user_input.command_type == 3) {
            execute_status_command(connection, &std_out_fd);
        }
        //if kill
        else if(user_input.command_type == 4){
            if(ack.status)
                printf("Job %d successfully terminated\n", ntohl(command_to_send.data.kill_data.job_id));
            else
                printf("Job %d does not exist on %s:%d\n", ntohl(command_to_send.data.kill_data.job_id), user_input.hostname, user_input.port);
        }
        //if copy
        else if(user_input.command_type == 5)
        {
            char *filenames_for_copy[MAX_FILES_COPY];

            //if client to server
            if(command_to_send.data.copy_data.copy_transition == Client_to_Server)
            {
                //copy all file names
                for(int i=2, j=0; i < argc-1; i++, j++)
                {
                    filenames_for_copy[j] = argv[i];
                }
            }
            //if server to client
            else if(command_to_send.data.copy_data.copy_transition == Server_to_Client)
            {
                //get last argument
                filenames_for_copy[0] = argv[argc-1];
            }

            execute_copy_process(connection, command_to_send.data.copy_data, ack, filenames_for_copy);
        }
        //if chdir
        else if(user_input.command_type == 6){
            if(ack.status)
                printf("Directory changed\n");
            else
                printf("Directory failed to change\n");
        }
        //if batch
        else
            printf("Job scheduled.\nUnique ID is %d.\n", ntohl(ack.job_id));

    }
    else if(rbytes == 0)
    {
        fprintf(stderr,"Server is not available\n");
    }

    // All done, close socket
    close(connection->sockfd);

    return 0;
}
