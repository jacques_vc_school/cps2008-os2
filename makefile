rex: rex.c helpers/registry.c helpers/common.c commands/run.c commands/submit.c commands/status.c commands/kill.c commands/chdir.c commands/copy.c helpers/client_helper.c helpers/server_helper.c
	gcc -Wall -o rex rex.c helpers/registry.c helpers/common.c commands/run.c commands/submit.c commands/status.c commands/kill.c commands/chdir.c commands/copy.c helpers/client_helper.c helpers/server_helper.c -lpthread
	sudo cp rex /usr/local/bin

rexd: rexd.c helpers/registry.c helpers/common.c commands/run.c commands/submit.c commands/status.c commands/kill.c commands/chdir.c commands/copy.c helpers/client_helper.c helpers/server_helper.c helpers/leader_helper.c
	gcc -Wall -o rexd helpers/common.c helpers/registry.c rexd.c commands/run.c commands/submit.c commands/kill.c  commands/chdir.c commands/copy.c commands/status.c helpers/client_helper.c helpers/server_helper.c helpers/leader_helper.c -lpthread
	sudo cp rexd /usr/local/bin

